<?php

namespace mud\core\services;

use mud\core\helpers\Settings;
use mud\core\models\ShortUrlImpression as Model;
use mud\core\records\ShortUrlImpression as Record;
use RuntimeException;
use yii\base\Component;
use yii\db\Exception;

class ShortUrlImpressions extends Component
{
    /**
     * Saves a short URL impression if logging is enabled
     *
     * @param Model $model
     * @return void
     * @throws Exception
     */
    public function logImpression(Model $model): void
    {
        if ( ! Settings::config('logShortUrlImpressions'))
        {
            return;
        }
        
        $record = new Record($model->getAttributes());
        
        if ( ! $record->save())
        {
            throw new RuntimeException('stop');
        }
    }
}