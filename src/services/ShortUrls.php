<?php

namespace mud\core\services;

use craft\elements\Entry;
use craft\events\DefineHtmlEvent;
use craft\helpers\ElementHelper;
use craft\helpers\Queue;
use Illuminate\Support\Collection;
use mud\core\elements\EntryVeneer;
use mud\core\helpers\ShortUrl as Helper;
use mud\core\helpers\Template;
use mud\core\jobs\GenerateAllEntryUrls;
use mud\core\models\ShortUrl as Model;
use mud\core\records\ShortUrl as Record;
use RuntimeException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Component;
use yii\base\Exception;

class ShortUrls extends Component
{
    /**
     * Retrieves all Short Urls
     *
     * @return Collection
     * @throws Exception
     */
    public function getAll(): Collection
    {
        $models = [];

        Record::find()->limit(null)->collect()->each(function ($record) use (&$models)
        {
            $models[] = Helper::createModelFromRecord($record);
        });
        
        return collect($models);
    }

    /**
     * Retrieves a single Short Url record
     * 
     * @param array $attributes
     * @return Record|null
     */
    public function findRecord(array $attributes): ?Record
    {
        return Record::findOne($attributes);
    }

    /**
     * Populates a single Short Url model from a record
     * 
     * @param array $attributes
     * @return Model|null
     * @throws Exception
     */
    public function findModel(array $attributes): ?Model
    {
        $record = $this->findRecord($attributes);
        return $record ? Helper::createModelFromRecord($record) : null;
    }
    
    /**
     * Creates a new entry Short Url record if one doesn't already exist
     * 
     * @param Entry $entry
     * @return void
     * @throws Exception
     */
    public function generateEntryUrl(Entry $entry): void
    {
        /** @var EntryVeneer $entry */
        
        // quit if this is a draft or revision, or if this entry section or entry type is not configured for short URLs
        if (ElementHelper::isDraftOrRevision($entry) || ! $entry->hasShortUrlEnabled())
        {
            return;
        }
        
        // no action if a record already exists
        if ($this->findRecord(['entryId' => $entry->id]))
        {
            return;
        }
        
        $model = Helper::createModelFromEntry($entry);
        $record = new Record($model->getAttributes(['entryId', 'code']));
        
        if ( ! $record->save())
        {
            throw new RuntimeException('Failed to save ShortUrl record.');
        }
    }
    

    /**
     * Raises a job to generate short URLs for all entries
     * Needed in the following scenarios
     * - site updated (site url) TODO: not stored in the db so not necessary?
     * - plugin settings updated (new sections/entry types may have been enabled)
     * - section updated (has urls setting may have been enabled)
     * 
     * @return void
     */
    public function generateAllEntryUrls(): void
    {
        Queue::push(new GenerateAllEntryUrls());
    }

    /**
     * Adds sidebar content to an entry, with short URL and QR code
     *
     * @param DefineHtmlEvent $event
     * @return void
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function appendEntrySidebar(DefineHtmlEvent $event): void
    {
        /** @var EntryVeneer $entry */
        $entry = $event->sender;
        
        if ( ! $entry->hasShortUrlEnabled())
        {
            return;
        }
        
        $event->html .= Template::entryShortUrlSidebarTemplate($entry);
    }
}
