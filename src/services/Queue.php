<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use craft\helpers\UrlHelper;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use mud\core\Plugin;
use mud\core\records\QueueFailureNotification;
use Throwable;
use yii\base\Exception;
use yii\queue\ExecEvent;

class Queue extends Component
{
    /**
     * Sends a notification of Queue failure
     * Ensures that only one notification is sent per failed job to avoid spamming notifications
     *
     * @param ExecEvent $event
     * @return void
     * @throws GuzzleException
     * @throws JsonException
     * @throws Exception
     */
    public function handleError(ExecEvent $event): void
    {
        $error = $event->error instanceof Throwable ? $event->error->getMessage() : $event->error;
        $url = UrlHelper::cpUrl('utilities/queue-manager/' . $event->id);

        Craft::info("====================================", __METHOD__);
        Craft::info("Mud error notification handler detected error", __METHOD__);
        Craft::info($event->error, __METHOD__);
        Craft::info("Job ID failing = $event->id", __METHOD__);
        
        if ($record = QueueFailureNotification::findOne(['jobId' => $event->id]))
        {
            $record->occurrences++;
            
            Craft::info("Notification already sent, upping occurrence count to $record->occurrences", __METHOD__);    
        }
        else
        {
            Craft::info("No notifications yet sent, creating new one for job $event->id and triggering Slack notification", __METHOD__);
            
            $record = new QueueFailureNotification([
                'jobId' => $event->id,
                'occurrences' => 1
            ]);
            
            Plugin::getInstance()->slack->notify('A Craft queued job has failed.', "*Error:*\n```$error```", $url);
        }
        
        $record->save();

        Craft::info("====================================", __METHOD__);
    }
}