<?php

namespace mud\core\services;

use craft\helpers\App;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use yii\base\Exception;

class Slack extends Api
{
    /**
     * @inheritdoc
     */
    protected array $requestOptions = [
        RequestOptions::HEADERS => [
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ]
    ];
    
    /**
     * Constructor sets base url from the environment
     *
     * @param GuzzleClientInterface|null $client
     * @throws Exception
     */
    public function __construct(GuzzleClientInterface $client = null)
    {
        $this->baseUrl = App::env('SLACK_NOTIFICATION_WEBHOOK');
        parent::__construct($client);
    }

    /**
     * Sends a notification
     *
     * @param string $title
     * @param string $message
     * @param string|null $url
     * @return void
     * @throws Exception
     * @throws GuzzleException
     * @throws JsonException
     */
    public function notify(string $title, string $message, string $url = null): void
    {
        if ( ! $this->baseUrl)
        {
            return;
        }

        $siteName = App::env('SITE_NAME');
        $environment = App::env('ENVIRONMENT');

        $payload = [
            "blocks" => [[
                "type" => "section",
                "text" => [
                    "type" => "mrkdwn",
                    "text" => $title
                ]
            ], [
                "type" => "section",
                "fields" => [[
                    "type" => "mrkdwn",
                    "text" => "*Site:*\n$siteName"
                ], [
                    "type" => "mrkdwn",
                    "text" => "*Environment:*\n$environment"
                ], [
                    "type" => "mrkdwn",
                    "text" => $message
                ]]
            ]
            ]
        ];

        if ($url)
        {
            $payload['blocks'][] = [
                "type" => "section",
                "text" => [
                    "type" => "mrkdwn",
                    "text" => "*URL:*\n$url"
                ]
            ];
        }

        $this->post('', $payload);
    }
}