<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use craft\events\AuthorizationCheckEvent;
use craft\events\DefineHtmlEvent;
use craft\events\IndexKeywordsEvent;
use craft\events\RegisterElementSourcesEvent;
use craft\events\RegisterUserPermissionsEvent;
use mud\core\elements\EntryVeneer;
use mud\core\helpers\Blueprint as Helper;
use mud\core\helpers\Settings;
use mud\core\helpers\Template;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Event;
use yii\base\Exception;
use yii\web\ForbiddenHttpException;

class Blueprint extends Component
{
    /**
     * Prevents saving of blueprint entry types and validates that the title has been updated
     * on entries created from a blueprint
     * 
     * @param Event $event
     * @return void
     */
    public function handleEntryValidation(Event $event): void
    {
        if ($event->sender->type->id === Helper::getEntryTypeId())
        {
            $isNew = collect(Craft::$app->request->getSegments())->last() === 'new';

            if (! $isNew && (! $event->sender->isDraft || ! Craft::$app->request->isAjax))
            {
                $event->sender->addError('type', 'Select a blueprint to create your entry.');
            }

            return;
        }
        
        $blueprint = Helper::entryBlueprint($event->sender->id);

        if ($blueprint && $blueprint->title === $event->sender->title)
        {
            $event->sender->slug = null;
            $event->sender->addError('title', 'Please update the Title before saving.');
        }
    }

    /**
     * Prevents blueprint entries from being indexed in Craft's internal search
     * 
     * @param IndexKeywordsEvent $event
     * @return void
     */
    public function excludeFromSearch(IndexKeywordsEvent $event): void
    {
        if ( ! $event->element instanceof Entry)
        {
            return;
        }
        
        /** @var Entry $entry */
        $entry = $event->element;
        $blueprintSectionIds = Settings::config('blueprintSectionIds');
        $blueprintSectionId = $blueprintSectionIds[$entry->site->id] ?? null;
        
        $blueprintEntryTypeIds = Settings::config('blueprintEntryTypeIds');
        $blueprintEntryTypeId = $blueprintEntryTypeIds[$entry->site->id] ?? null;
        
        $event->isValid = $entry->sectionId !== (int) $blueprintSectionId && $entry->type->id !== (int) $blueprintEntryTypeId;
    }

    /**
     * Appends the blueprint sidebar to the entry edit page
     *
     * @param DefineHtmlEvent $event
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function appendEntrySidebar(DefineHtmlEvent $event): void
    {
        /** @var EntryVeneer $entry */
        $entry = $event->sender;
        
        if ( ! $blueprintSectionId = Helper::getSectionId())
        {
            return;
        }
        
        if ($blueprintSectionId === $entry->sectionId)
        {
            $event->html .= Template::blueprintSidebarTemplate($entry);
        }
    }

    /**
     * Appends blueprint buttons to the entry edit page
     * 
     * @param DefineHtmlEvent $event
     * @return void
     */
    public function appendEntryButtons(DefineHtmlEvent $event): void
    {
        /** @var EntryVeneer $entry */
        $entry = $event->sender;
        
        // quit if blueprint section not configured or we can't get the section
        if ((! $blueprintSectionId = Helper::getSectionId()) || (! $blueprintSection = Craft::$app->entries->getSectionById($blueprintSectionId)))
        {
            return;
        }
        
        // if the user doesn't have permission to view blueprint entries
        if ( ! Helper::userHasPermission() || ! Craft::$app->user->checkPermission('viewEntries:'. $blueprintSection->uid))
        {
            return;
        }
        
        // or if we're in the blueprint section
        if ($entry->sectionId === $blueprintSectionId)
        {
            return;
        }
        
        // show source blueprint if this entry has been created from one
        if ($blueprint = Helper::entryBlueprint($entry->id))
        {
            $event->html .= Template::blueprintViewSourceButtonTemplate($blueprint);
            return;
        }
        
        // show create from blueprint button if the blueprint section can take this kind of entry and this entry isn't a draft
        $blueprintEntryTypes = collect($blueprintSection->entryTypes)->pluck('id')->toArray();
        
        if ($entry->getIsDraft() || ! in_array($entry->type->id, $blueprintEntryTypes, true))
        {
            return;
        }

        $event->html .= Template::blueprintCreateFromEntryButtonTemplate($entry);
    }

    /**
     * Registers the manageBlueprints permission
     * 
     * @param RegisterUserPermissionsEvent $event
     * @return void
     */
    public function registerPermissions(RegisterUserPermissionsEvent $event): void
    {
        $event->permissions[] = [
            'heading' => 'Blueprints',
            'permissions' => [
                'manageBlueprints' => [
                    'label' => 'Manage blueprints',
                    'info' => 'This permission allows users to manage blueprint entries.',
                    'warning' => 'To create entries from blueprints, users should have full access to the blueprints section. This permission is for managing the blueprints themselves.',
                ],
            ],
        ];
    }

    /**
     * Authorises view access to blueprint entries in the cp
     * 
     * @param AuthorizationCheckEvent $event
     * @return void
     */
    public function authoriseView(AuthorizationCheckEvent $event): void
    {
        if ( ! $event->element instanceof Entry || ! $sectionId = Helper::getSectionId())
        {
            return;
        }
        
        /** @var Entry $entry */
        $entry = $event->element;
        
        $event->authorized = $entry->sectionId !== $sectionId || Helper::userHasPermission();
    }

    /**
     * Removes blueprint sources from the element index
     * 
     * @param RegisterElementSourcesEvent $event
     * @return void
     */
    public function sanitiseEntrySources(RegisterElementSourcesEvent $event): void
    {
        if (Helper::userHasPermission() || ! $blueprintSectionId = Helper::getSectionId())
        {
            return;
        }
        
        $event->sources = collect($event->sources)
            ->filter(function (array $source) use ($blueprintSectionId) {
                return ! (array_key_exists('criteria', $source) && $source['criteria'] && array_key_exists('sectionId', $source['criteria'])) || $source['criteria']['sectionId'] !== (int) $blueprintSectionId;
            })
            ->map(function (array $source) use ($blueprintSectionId) {
                if (array_key_exists('criteria', $source) && $source['criteria'] && is_array($source['criteria']['sectionId'])) {
                    $source['criteria']['sectionId'] = collect($source['criteria']['sectionId'])
                        ->filter(function ($value) use ($blueprintSectionId) {
                            return $value !== (int) $blueprintSectionId;
                        })
                        ->toArray();
                }
                return $source;
            })->toArray();
    }

    /**
     * Checks if the current user has permission to view blueprint entries on the frontend
     * 
     * @return void
     * @throws ForbiddenHttpException
     */
    public function checkViewPermissions(): void
    {
        if ( ! Craft::$app->request->isSiteRequest || ! $blueprintSectionId = Helper::getSectionId()) 
        {
            return;
        }
        
        $element = Craft::$app->urlManager->getMatchedElement();
        
        if ( ! $element instanceof Entry || $element->sectionId !== $blueprintSectionId)
        {
            return;
        }
        
        if ( ! Helper::userHasPermission())
        {
            throw new ForbiddenHttpException('You do not have permission to view blueprint entries.');
        }
    }
}