<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use Laminas\Feed\Reader\Http\Psr7ResponseDecorator;
use RuntimeException;

abstract class Api extends Component
{
    /**
     * Guzzle client
     *
     * @var Client|GuzzleClientInterface
     */
    protected GuzzleClientInterface|Client $client;

    /**
     * @var string Base URL for the API
     */
    protected string $baseUrl = '';

    /**
     * @var array Guzzle request options
     */
    protected array $requestOptions = [];

    /**
     * Constructor sets up the client
     *
     * @param GuzzleClientInterface|null $client
     */
    public function __construct(GuzzleClientInterface $client = null)
    {
        $this->client = $client ?: Craft::createGuzzleClient([ 'base_uri' => $this->baseUrl ]);
        parent::__construct();
    }

    /**
     * Makes a request
     *
     * @param string $method
     * @param string $endpoint
     * @param array $successCodes
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function request(string $method, string $endpoint = '', array $successCodes = [ 200 ]): mixed
    {
        Craft::info("Sending $method request to $endpoint with options:", __METHOD__);
        Craft::info(json_encode($this->requestOptions, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT), __METHOD__);

        $response = new Psr7ResponseDecorator($this->client->request($method, $endpoint, $this->requestOptions));

        Craft::info("Received response with status code {$response->getStatusCode()}", __METHOD__);

        if ( ! in_array($response->getStatusCode(), $successCodes, true))
        {
            throw new RuntimeException("Request to endpoint '$endpoint' failed, got response code {$response->getStatusCode()}.");
        }

        try 
        {
            $body = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            Craft::info("Response body:", __METHOD__);
            Craft::info(json_encode($body, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT), __METHOD__);
        } 
        catch (JsonException $e) 
        {
            $body = null;
            Craft::info("Response body not parsed as JSON, got error: {$e->getMessage()}", __METHOD__);
        }
        
        Craft::info('==================', __METHOD__);

        return $body;
    }

    /**
     * Makes a GET request
     *
     * @param string $endpoint
     * @param array $payload
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function get(string $endpoint = '', array $payload = []): mixed
    {
        if ($payload)
        {
            $endpoint .= '?' . http_build_query($payload);
        }

        return $this->request('GET', $endpoint);
    }

    /**
     * Makes a POST request
     *
     * @param string $endpoint
     * @param array $payload
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function post(string $endpoint = '', array $payload = []): mixed
    {
        $this->requestOptions = array_merge($this->requestOptions, [
            RequestOptions::BODY => json_encode($payload, JSON_THROW_ON_ERROR)
        ]);

        return $this->request('POST', $endpoint, [ 200, 201 ]);
    }

    /**
     * Performs a DELETE request
     *
     * @param string $endpoint
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function delete(string $endpoint): mixed
    {
        return $this->request('DELETE', $endpoint);
    }

    /**
     * Performs a PUT request
     *
     * @param string $endpoint
     * @param array|null $payload
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public function put(string $endpoint, array $payload = null): mixed
    {
        $this->requestOptions = array_merge($this->requestOptions, [
            RequestOptions::BODY => json_encode($payload, JSON_THROW_ON_ERROR)
        ]);

        return $this->request('PUT', $endpoint, [ 200, 201 ]);
    }
}