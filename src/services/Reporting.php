<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use craft\helpers\App;
use yii\base\NotSupportedException;

class Reporting extends Component
{
    /**
     * Compiles instance data
     * 
     * @return array
     * @throws NotSupportedException
     */
    public function getInstanceData(): array
    {
        return [
            'server' => [
                'os' => [
                    'type' => PHP_OS,
                    'version' => php_uname('r')
                ],
                'php' => App::phpVersion(),
                'database' => [
                    'driver' => Craft::$app->db->getDriverLabel(),
                    'version' => App::normalizeVersion(Craft::$app->db->getSchema()->getServerVersion()),
                ]
            ],
            'craft' => [
                'version' => Craft::$app->version,
                'edition' => Craft::$app->edition->name,
                'licence' => App::licenseKey()
            ],
            'plugins' => Craft::$app->plugins->getAllPluginInfo(),
            'updates' => Craft::$app->updates->getUpdates()
        ];
    }
}