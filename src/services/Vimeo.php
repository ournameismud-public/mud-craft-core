<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\errors\AssetException;
use craft\events\DefineHtmlEvent;
use craft\helpers\App;
use Exception;
use JsonException;
use mud\core\elements\AssetVeneer;
use mud\core\helpers\Settings;
use mud\core\helpers\Template;
use mud\core\helpers\Vimeo as Helper;
use mud\core\models\VimeoFiles as VimeoFilesModel;
use mud\core\records\VimeoFiles as VimeoFilesRecord;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Vimeo\Exceptions\VimeoRequestException;
use Vimeo\Vimeo as Client;
use yii\base\Exception as YiiBaseException;
use yii\base\InvalidConfigException;
use yii\db\Exception as YiiDbException;
use yii\web\UnauthorizedHttpException;

class Vimeo extends Component
{
    /**
     * @var Client|null Vimeo API client
     */
    protected ?Client $client = null;
    
    /*
     * Vimeo constructor initiates and authorizes the Vimeo API client
     */
    /**
     * @throws YiiBaseException
     * @throws UnauthorizedHttpException
     */
    public function __construct()
    {
        if (Settings::config('enableVimeoApi'))
        {
            $clientId = App::env('VIMEO_CLIENT_ID');
            $clientSecret = App::env('VIMEO_CLIENT_SECRET');
            $accessToken = App::env('VIMEO_ACCESS_TOKEN');

            if ( ! $clientId || ! $clientSecret || ! $accessToken)
            {
                throw new UnauthorizedHttpException('Vimeo API credentials not set.');
            }

            $this->client = new Client($clientId, $clientSecret, $accessToken);   
        }
        
        parent::__construct();
    }

    /**
     * Retrieves video files data from Vimeo API
     * 
     * @param int $vimeoId
     * @return array
     * @throws UnauthorizedHttpException
     * @throws JsonException
     * @throws VimeoRequestException
     */
    public function getVideoFiles(int $vimeoId): array
    {
        Craft::info("Querying Vimeo API for '/videos/$vimeoId?fields=files'", __METHOD__);
        
        $response = $this->client->request("/videos/$vimeoId?fields=files");
        $status = $response['status'];
        
        if ($status !== 200)
        {
            throw new UnauthorizedHttpException("Failed to get video files due to $status response.");   
        }
        
        Craft::info("Response:", __METHOD__);
        Craft::info(json_encode($response, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT), __METHOD__);
    
        // body is empty if not in our account.
        if ( ! array_key_exists('body', $response) || ! array_key_exists('files', $response['body']))
        {
            Craft::info('Vimeo API returned successful response but no files found for video. Could indicate that this video is not accessible for this integration.', __METHOD__);
            return [];
        }

        Craft::info('==================', __METHOD__);
        
        return $response['body'];
    }

    /**
     * Saves video files data to the database
     * 
     * @param int $assetId
     * @param int $vimeoId
     * @param array $embedData
     * @param array $filesData
     * @return bool|string
     * @throws JsonException
     * @throws YiiDbException
     */
    public function saveVideoFilesData(int $assetId, int $vimeoId, array $embedData, array $filesData): bool|string
    {
        $model = Helper::populateFilesModel($assetId, $vimeoId, $embedData, $filesData);
        $record = new VimeoFilesRecord($model->getAttributes());

        if ( ! $record->save())
        {
            return 'Unable to save vimeo files data.';
        }
        
        return true;
    }

    /**
     * Handles an asset save event by retrieving API data if the asset is a Vimeo video
     * 
     * @param Asset $asset
     * @return void
     * @throws JsonException
     * @throws UnauthorizedHttpException
     * @throws VimeoRequestException
     * @throws YiiDbException
     * @throws AssetException
     * @throws InvalidConfigException
     */
    public function handleAssetSave(Asset $asset): void
    {
        /** @var AssetVeneer $asset */
        
        if ($asset->kind !== 'json' || ! Helper::isEnabled() || VimeoFilesRecord::findOne(['assetId' => $asset->id]))
        {
            return;
        }
        
        if ( ! $embedData = $asset->embedData())
        {
            return;
        }
        
        if ( ! $videoId = Helper::getVimeoIdFromUrl($embedData['url']))
        {
            return;
        }
        
        $videoFiles = $this->getVideoFiles($videoId);
        $this->saveVideoFilesData($asset->id, $videoId, $embedData, $videoFiles);
    }

    /**
     * Populates a single Vimeo Files Data model from a record
     *
     * @param array $attributes
     * @return VimeoFilesModel|null
     * @throws Exception
     */
    public function findFilesModel(array $attributes): ?VimeoFilesModel
    {
        $record = VimeoFilesRecord::findOne($attributes);
        return $record ? Helper::createFilesModelFromRecord($record) : null;
    }

    /**
     * Appends Vimeo files data to the asset sidebar
     *
     * @param DefineHtmlEvent $event
     * @return void
     * @throws AssetException
     * @throws InvalidConfigException
     * @throws JsonException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws YiiBaseException
     * @throws Exception
     */
    public function appendAssetSidebar(DefineHtmlEvent $event): void
    {
        /** @var AssetVeneer $asset */
        $asset = $event->sender;
        
        if ($asset->kind !== 'json' || ! Helper::isEnabled())
        {
            return;
        }
        
        $model = $asset->filesData();
        $vimeoId = $model?->vimeoId;
        
        if ( ! $vimeoId && $embedData = $asset->embedData())
        {
            $vimeoId = Helper::getVimeoIdFromUrl($embedData['url']);
        }
        
        if (! $vimeoId)
        {
            return;
        }

        $event->html .= Template::vimeoFilesSidebarTemplate($asset, $vimeoId, $model);
    }

    /**
     * Removes all files data for a given asset
     * 
     * @param int $assetId
     * @return void
     */
    public function removeFiles(int $assetId): void
    {
        VimeoFilesRecord::deleteAll(['assetId' => $assetId]);
    }
}