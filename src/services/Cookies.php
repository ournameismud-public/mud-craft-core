<?php

namespace mud\core\services;

use craft\base\Component;
use craft\errors\SiteNotFoundException;
use JsonException;
use mud\core\helpers\Cookies as Helper;
use mud\core\records\CookiePreferenceLog;
use yii\db\Exception;

class Cookies extends Component
{
    /**
     * Saves a new cookie preference log record
     *
     * @param array $preferences
     * @return bool|string
     * @throws JsonException
     * @throws SiteNotFoundException
     * @throws Exception
     */
    public function savePreferenceLog(array $preferences): bool|string
    {
        $model = Helper::populateCookiePreferenceLogModel($preferences);
        
        if ( ! $model->validate())
        {
            return 'Cookie preference log model validation failed.';
        }
        
        $record = new CookiePreferenceLog($model->getAttributes()); 
        
        if ( ! $record->save())
        {
            return 'Unable to save cookie preference log.';
        }
        
        return true;
    }
}