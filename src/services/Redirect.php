<?php

namespace mud\core\services;

use Craft;
use craft\base\Component;
use craft\helpers\Console;
use mud\core\helpers\Redirect as Helper;
use mud\core\records\Redirect as Record;
use yii\base\Event;
use yii\base\ExitException;
use yii\base\InvalidRouteException;
use yii\helpers\BaseConsole;

class Redirect extends Component
{
    /**
     * Redirects a request to a new location if a match is found
     * 
     * @throws ExitException
     * @throws InvalidRouteException
     */
    public function handleRequest(Event $event): void
    {
        $request = $event->sender->request;
        
        if (($request->isSiteRequest || $request->isCpRequest) && $redirect = Helper::findExactMatch(Craft::$app->sites->currentSite->id, $request->url))
        {
            Craft::info("Redirecting request to {$redirect->destinationPath} with status {$redirect->httpStatus}", __METHOD__);
            Craft::$app->response->redirect($redirect->destinationPath, $redirect->httpStatus)->send();
            Craft::$app->end();
        }
    }

    /**
     * Handles garbage collection for the redirect records
     * 
     * @return void
     */
    public function handleGarbageCollection(): void
    {
        if ($isConsole = Craft::$app->request->isConsoleRequest) 
        {
            Console::stdout(sprintf('    > deleting old redirects from the `%s` table ... ', Record::tableName()));
        }
        
        Helper::deleteOldRecords();

        if ($isConsole)
        {
            Console::stdout("done\n", BaseConsole::FG_GREEN);
        }
    }
}