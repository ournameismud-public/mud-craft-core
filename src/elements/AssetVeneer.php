<?php

namespace mud\core\elements;

use craft\elements\Asset;
use mud\core\behaviors\Vimeo as VimeoBehavior;

/**
 * Veneer wrapper for the Asset class in order to recognise and autocomplete behaviors in PhpStorm
 * https://nystudio107.com/blog/autocomplete-for-behaviors-in-phpstorm-with-mixins-veneers
 * 
 * @mixin VimeoBehavior
 */
class AssetVeneer extends Asset
{
}