<?php

namespace mud\core\elements;

use craft\elements\Entry;
use mud\core\behaviors\ShortUrl as ShortUrlBehavior;

/**
 * Veneer wrapper for the Entry class in order to recognise and autocomplete behaviors in PhpStorm
 * https://nystudio107.com/blog/autocomplete-for-behaviors-in-phpstorm-with-mixins-veneers
 * 
 * @mixin ShortUrlBehavior
 */
class EntryVeneer extends Entry
{
}