<?php

namespace mud\core\jobs;

use craft\queue\BaseJob;
use mud\core\models\ShortUrlImpression as Model;
use mud\core\Plugin;
use RuntimeException;
use yii\db\Exception;

class LogShortUrlImpression extends BaseJob
{
    /**
     * @inheritDoc
     */
    public ?string $description = 'Logging short URL impression';

    /**
     * @var int Short URL ID
     */
    public int $urlId;

    /**
     * @var int Site ID
     */
    public int $siteId;

    /**
     * @var int User ID
     */
    public int $userId;

    /**
     * @var int Request status code
     */
    public int $status;

    /**
     * @var string Request status reason
     */
    public string $reason;

    /**
     * @var string Request user agent 
     */
    public string $useragent;

    /**
     * Logs a short URL impression
     *
     * @param $queue
     * @return void
     * @throws Exception
     */
    public function execute($queue): void
    {
        $model = new Model([
            'urlId' => $this->urlId,
            'siteId' => $this->siteId,
            'userId' => $this->userId,
            'status' => $this->status,
            'reason' => $this->reason,
            'useragent' => $this->useragent
        ]);
        
        if ( ! $model->validate())
        {
            throw new RuntimeException('ShortUrlImpression validation failed.');
        }
        
        Plugin::getInstance()->shortUrlImpressions->logImpression($model);
    }
}