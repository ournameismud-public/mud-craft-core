<?php

namespace mud\core\jobs;

use Craft;
use craft\queue\BaseJob;
use mud\core\helpers\ShortUrl as Helper;
use mud\core\Plugin;
use yii\base\Exception;

class GenerateAllEntryUrls extends BaseJob
{
    /**
     * @inheritDoc
     */
    public ?string $description = 'Generating entry short URLs';

    /**
     * Generates short URLs for a collection of entries
     *
     * @param $queue
     * @return void
     * @throws Exception
     */
    public function execute($queue): void
    {
        $entries = Helper::entriesWithoutShortUrlsQuery()->collect();
        $total = $entries->count();

        foreach ($entries as $i => $entry) 
        {
            $this->setProgress(
                $queue,
                $i / $total,
                Craft::t('app', '{step, number} of {total, number}', [
                    'step' => $i + 1,
                    'total' => $total,
                ])
            );
            
            Plugin::getInstance()->shortUrls->generateEntryUrl($entry);
        }
    }
}