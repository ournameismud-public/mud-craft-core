<?php

namespace mud\core\controllers;

use Craft;
use craft\errors\SiteNotFoundException;
use craft\web\Controller;
use JsonException;
use mud\core\helpers\Settings;
use mud\core\helpers\Template;
use mud\core\Plugin;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

class CookiesController extends Controller
{
    /**
     * @inheritdoc
     */
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_LIVE;

    /**
     * Loads the cookie modal template in a JSON request
     * 
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function actionLoadTemplate(): Response
    {
        $this->requireAcceptsJson();
        
        $markup = Template::loadTemplateFromConfig('cookiesModalTemplatePath');
        
        return $this->asSuccess(null, [
            'markup' => $markup,
            'blockingMode' => Settings::config('cookiesDisplayBlockingMode')
        ]);
    }

    /**
     * Handles a request to save references to the cookie log
     *
     * @return Response
     * @throws BadRequestHttpException
     * @throws JsonException
     * @throws MethodNotAllowedHttpException
     * @throws SiteNotFoundException
     * @throws \yii\db\Exception
     */
    public function actionSavePreferencesLog(): Response
    {
        $this->requirePostRequest();
        $this->requireAcceptsJson();
        
        $preferences = Craft::$app->getRequest()->getRequiredBodyParam('preferences');
        $saved = Plugin::getInstance()->cookies->savePreferenceLog($preferences);
        
        return $saved === true ? $this->asSuccess() : $this->asFailure($saved);
    }
}