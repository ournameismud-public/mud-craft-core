<?php

namespace mud\core\controllers;

use Craft;
use craft\errors\MissingComponentException;
use craft\web\Controller;
use Throwable;
use yii\web\BadRequestHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

class CacheController extends Controller
{
    /**
     * @inheritdoc
     */
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_LIVE;

    /**
     * Returns the current user's permissions
     * 
     * @return Response
     * @throws Throwable
     * @throws MissingComponentException
     * @throws BadRequestHttpException
     * @throws MethodNotAllowedHttpException
     */
    public function actionGetUserPermissions(): Response
    {
        $this->requireAcceptsJson();
        $this->requirePostRequest();

        $user = Craft::$app->getUser()->getIdentity();

        return $this->asSuccess(null, [
            'userId' => $user->id,
            'cpAccess' => $user->can('accessCp')
        ]);
    }
}