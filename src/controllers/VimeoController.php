<?php

namespace mud\core\controllers;

use Craft;
use craft\errors\MissingComponentException;
use craft\web\Controller;
use mud\core\Plugin;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class VimeoController extends Controller
{
    /**
     * @inheritdoc 
     */
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_NEVER;

    /**
     * Removes a files record
     * 
     * @return Response
     * @throws MissingComponentException
     * @throws BadRequestHttpException
     */
    public function actionRemoveFiles(): Response
    {
        $this->requireCpRequest();
        
        $assetId = Craft::$app->request->getRequiredQueryParam('assetId');
        Plugin::getInstance()->vimeo->removeFiles($assetId);
        
        Craft::$app->getSession()->setSuccess(Craft::t('app', 'Vimeo data deleted.'));
        
        return $this->redirect(Craft::$app->request->referrer);
    }
}
