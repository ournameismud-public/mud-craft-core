<?php

namespace mud\core\controllers;

use Craft;
use craft\elements\Entry;
use craft\errors\InvalidElementException;
use craft\errors\UnsupportedSiteException;
use craft\models\Section;
use craft\web\Controller;
use mud\core\helpers\Blueprint as Helper;
use mud\core\helpers\Redirect;
use RuntimeException;
use Throwable;
use yii\base\Exception;
use yii\db\Exception as DbException;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BlueprintController extends Controller
{
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_NEVER;

    /**
     * Redirects to the placeholder entry with an error message
     * and deletes the duplicate entry if it exists
     *
     * @param Entry|null $duplicate
     * @param Entry $placeholder
     * @param string $message
     * @return Response
     * @throws Throwable
     */
    protected function bail(?Entry $duplicate, Entry $placeholder, string $message): Response
    {
        if ($duplicate && ! Craft::$app->elements->deleteElement($duplicate, true))
        {
            throw new RuntimeException('Failed to delete entry');
        }
        
        Craft::$app->session->setError($message);
        
        return $this->redirect($placeholder->cpEditUrl);
    }

    /**
     * Creates a new entry from a blueprint
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws InvalidElementException
     * @throws UnsupportedSiteException
     * @throws Exception
     * @throws DbException
     * @throws BadRequestHttpException
     */
    public function actionCreateEntry(): Response
    {
        $this->requireCpRequest();
        
        $sourceId = Craft::$app->request->getRequiredQueryParam('sourceId');
        $placeholderId = Craft::$app->request->getRequiredQueryParam('placeholderId');
        
        $source = Craft::$app->entries->getEntryById($sourceId);
        $placeholder = Craft::$app->entries->getEntryById($placeholderId);
        
        if ( ! $source || ! $placeholder)
        {
            throw new NotFoundHttpException('Source or target entry not found.');
        }
        
        $duplicate = Helper::createEntryFromBlueprint($source, $placeholder, Craft::$app->user->getIdentity()->id);
        $record = Helper::createRecord($sourceId, $duplicate->id);

        // if we're dealing with a structure, move the new entry before the placeholder
        if (($placeholder->section->type === Section::TYPE_STRUCTURE) && ! Craft::$app->getStructures()->moveBefore($placeholder->structureId, $duplicate, $placeholder)) 
        {
            $record->delete();
            return $this->bail($duplicate, $placeholder, 'Failed to move entry into structure.');
        }
        
        // tidy up
        // can't disable the placeholder as that still appears in structure when you change parent in the UI
        // so delete it and then add a redirect so we don't break the browser history
        Redirect::createBlueprintPlaceholderRedirect($placeholder->siteId, Craft::$app->request->referrer);
        Craft::$app->elements->deleteElement($placeholder, true);

        Craft::$app->session->setSuccess('Draft entry created from Blueprint.');
        
        // and away we go
        return $this->redirect($duplicate->cpEditUrl);
    }

    /**
     * Creates a new blueprint from an entry
     * NB dupe will fail if validation of clone fails, so be mindful of required fields on entry types
     * 
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws ForbiddenHttpException
     */
    public function actionCreateBlueprintFromEntry(): Response
    {
        $this->requireCpRequest();
        $this->requirePermission('manageBlueprints');
        
        $sourceId = Craft::$app->request->getRequiredQueryParam('sourceId');
        $blueprintSectionId = Helper::getSectionId();
        
        if ( ! $source = Craft::$app->entries->getEntryById($sourceId))
        {
            throw new NotFoundHttpException('Source entry not found.');
        }
        
        $blueprint = Helper::duplicateEntry($source, Craft::$app->user->getIdentity()->id, $blueprintSectionId);

        Craft::$app->session->setSuccess('Draft Blueprint created from entry.');
        
        return $this->redirect($blueprint->cpEditUrl);
    }
}