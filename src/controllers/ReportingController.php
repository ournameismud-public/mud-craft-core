<?php

namespace mud\core\controllers;

use craft\web\Controller;
use mud\core\Plugin;
use yii\base\NotSupportedException;
use yii\web\Response;

/**
 * Reporting controller
 */
class ReportingController extends Controller
{
    /**
     * @inheritdoc
     */
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_LIVE;

    /**
     * Handles a request for Craft instance data
     * 
     * @return Response
     * @throws NotSupportedException
     */
    public function actionIndex(): Response
    {
        $data = Plugin::getInstance()->reporting->getInstanceData();
        return $this->asJson($data);
    }
}
