<?php

namespace mud\core\controllers;

use Craft;
use craft\elements\Entry;
use craft\errors\MissingComponentException;
use craft\helpers\Queue;
use craft\web\Controller;
use mud\core\elements\EntryVeneer;
use mud\core\jobs\LogShortUrlImpression;
use mud\core\models\ShortUrl as Model;
use mud\core\Plugin;
use Throwable;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ShortUrlController extends Controller
{
    /**
     * @inheritdoc 
     */
    protected array|int|bool $allowAnonymous = self::ALLOW_ANONYMOUS_LIVE;

    /**
     * Raises a job to log a short url impression
     * 
     * @param Model $model
     * @param int $status
     * @param string $reason
     * @return void
     * @throws Throwable
     */
    protected function logImpression(Model $model, int $status, string $reason): void
    {
        $request = Craft::$app->getRequest();
        $user = Craft::$app->getUser()->getIdentity();
        
        if ( ! $request->userAgent)
        {
            return;
        }
        
        $job = new LogShortUrlImpression([
            'urlId' => $model->id,
            'siteId' => $request->sites->currentSite->id,
            'userId' => $user->id ?? null,
            'status' => $status,
            'reason' => $reason,
            'useragent' => $request->userAgent,
        ]);
        
        Queue::push($job);
    }

    /**
     * Handles short url not found
     * 
     * @param Model $model
     * @param string $reason
     * @return void
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    protected function handleNotFound(Model $model, string $reason): void
    {
        $this->logImpression($model, 404, $reason);
        throw new NotFoundHttpException($reason);
    }

    /**
     * Resolves a Short URL request
     *
     * @param string $code
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionIndex(string $code): Response
    {
        if ( ! $model = Plugin::getInstance()->shortUrls->findModel(['code' => $code]))
        {
            throw new NotFoundHttpException('Short url not found.');
        }

        /* @var $entry EntryVeneer */
        if ( ! $entry = Entry::find()->site('*')->id($model->entryId)->one())
        {
            $this->handleNotFound($model, 'Entry not found.');
        }

        if ( ! $entry->hasShortUrlEnabled()) /** @phpstan-ignore-line */
        {
            $this->handleNotFound($model, 'Entry type not in scope.');
        }
        
        if ( ! $entry->url)
        {
            $this->handleNotFound($model, 'Entry has no URL.');
        }
        
        if ($entry->status !== Entry::STATUS_LIVE || ! $entry->enabled)
        {
            $this->handleNotFound($model, 'Entry is not live.');
        }
        
        $this->logImpression($model, 200, 'OK');
        
        return $this->redirect("$entry->url?utm_medium=shorturl");
    }

    /**
     * Handles a request generate any missing entry short urls
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws MissingComponentException
     * @throws MethodNotAllowedHttpException
     */
    public function actionGenerateAllEntryUrls(): Response
    {
        $this->requirePostRequest();
        $this->requirePermission('utility:short-url');
        
        Plugin::getInstance()->shortUrls->generateAllEntryUrls();
        Craft::$app->getSession()->setNotice(Craft::t('app', 'Short URLs will be generated via the queue.'));
        
        return $this->redirect(Craft::$app->request->referrer);
    }
}
