class MudCache {
	
	/**
	 * Constructor sets up properties and calls init() when the DOM is ready
	 */
	constructor() {
		
		// base instance
		this.base = new MudBase()

		// markup data attributes
		this.dataAttributes = {
			wrapper: 'data-mud-craft-core-cache-notice-wrapper',
		};

		// ui
		this.wrapper = null;
		
		// initiate when dom ready
		if (document.readyState === 'complete') this.init();
		else window.addEventListener('DOMContentLoaded', () => this.init());
	}

	/**
	 * Initiates the component when the DOM is ready
	 */
	async init() {

		if (typeof Craft !== 'undefined') return; // don't run automatically on CP pages
		
		this.wrapper = document.querySelector(`[${this.dataAttributes.wrapper}]`);
		
		if (!this.wrapper) this.base.debug('MudCache: cache notice wrapper not found.');
		else if (await this.detectionAllowed())
		{
			const cachedAt = this.getCacheTimestamp();
			if (cachedAt) this.wrapper.innerText = `This is a cached page, created on ${cachedAt}.`;
		}
		
	}

	/**
	 * Checks if the user is allowed to detect cache
	 * 
	 * @returns {Promise<*|boolean>}
	 */
	async detectionAllowed() {
		
		await this.base.getSession();
		
		if (this.base.session.isGuest) return false;

		const user = await this.base.doFetch('/mud/cache/session', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'X-CSRF-Token': this.base.session.csrfTokenValue,
			},
		});
		
		return user.cpAccess;
		
	}

	/**
	 * Extracts the cache timestamp from the document
	 * 
	 * @returns {null|string}
	 */
	getCacheTimestamp() {
		const pattern = /Cached by Blitz on (\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[-+]\d{2}:\d{2})/;
		for (const node of document.childNodes) {
			if (node.nodeType === 8 && pattern.test(node.nodeValue)) {
				return this.formatTimestamp(new Date(node.nodeValue.match(pattern)[1]));
			}
		}
	}

	/**
	 * Formats the timestamp
	 * 
	 * @param timestamp
	 * @returns {string}
	 */
	formatTimestamp(timestamp) {
		const date = new Date(timestamp);
		const options = { hour: '2-digit', minute: '2-digit', day: '2-digit', month: 'long', year: 'numeric' };
		return date.toLocaleDateString('en-GB', options);
	}
}

new MudCache();
