class MudBase {

	/**
	 * Constructor sets up properties
	 */
	constructor() {
		this.session = null;
	}

	/**
	 * Makes a fetch request and handles the response
	 * 
	 * @param path
	 * @param options
	 * @returns {Promise<any|string>}
	 */
	async doFetch(path, options) {
		
		try {
			
			const res = await fetch(path, options);
			
			if ( ! res.ok) {
				const detail = options.headers.Accept === 'application/json' ? await res.json() : await res.text();
				this.handleError(`${res.status} - ${detail.message}`);
			}
			
			return options.headers.Accept === 'application/json' ? await res.json() : await res.text();
			
		} catch (err) {
			
			this.handleError(err);
			
		}
		
	}

	/**
	 * Gets the Craft session info
	 * 
	 * @returns {Promise<void>}
	 */
	async getSession() {
		this.session = await this.doFetch('/actions/users/session-info', {
			headers: {
				Accept: 'application/json'
			}
		});
	}

	/**
	 * Handles an error
	 *
	 * @param msg
	 */
	handleError(msg) {
		throw new Error(msg);
	}

	/**
	 * Logs a message if the debug query string is present
	 * 
	 * @param msg
	 */
	debug(msg) {
		const params = new URLSearchParams(window.location.search);
		if (params.get('debug') === '1') console.log(msg);
	}

	/**
	 * Sets a cookie
	 *
	 * @param cookieName
	 * @param value
	 * @param expiry
	 */
	setCookie(cookieName, value, expiry) {
		const date = new Date();
		date.setTime(date.getTime() + expiry * 24 * 60 * 60 * 1000);
		const expires = 'expires=' + date.toUTCString();
		document.cookie =
			cookieName +
			'=' +
			JSON.stringify(value) +
			';' +
			expires +
			'; path=/';
	}

	/**
	 * Gets a cookie value
	 *
	 * @param cookieName
	 * @returns {any|null}
	 */
	getCookieValue(cookieName) {
		const name = cookieName + '=';
		const decodedCookie = decodeURIComponent(document.cookie);
		const cookieArray = decodedCookie.split(';');
		for (let i = 0; i < cookieArray.length; i++) {
			let cookie = cookieArray[i];
			while (cookie.charAt(0) === ' ') {
				cookie = cookie.substring(1);
			}
			if (cookie.indexOf(name) === 0) {
				return JSON.parse(cookie.substring(name.length, cookie.length));
			}
		}
		return null;
	}
	
}
