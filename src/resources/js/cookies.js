// Define dataLayer and the gtag function to enable pushing events to Google Tag Manager
// https://developers.google.com/tag-platform/security/guides/consent
// https://developers.google.com/tag-platform/security/concepts/cmp
// https://developers.google.com/tag-platform/tag-manager/templates/consent-apis?sjid=3235211138507787495-EU
window.dataLayer = window.dataLayer || [];
function gtag() {
	dataLayer.push(arguments);
}

class MudCookies {
	
	/**
	 * Constructor sets up properties and calls init() when the DOM is ready
	 */
	constructor() {
		
		// base instance
		this.base = new MudBase();
		
		// cookies
		this.cookieName = 'mud-craft-core-cookies';
		this.cookieExpiry = 365;

		// markup data attributes
		this.dataAttributes = {
			wrapper: 'data-mud-craft-core-cookie-wrapper',
			hiddenClasses: 'data-mud-craft-core-cookie-hidden-classes',
			visibleClasses: 'data-mud-craft-core-cookie-visible-classes',
			isExpandedClasses: 'data-mud-craft-core-cookie-is-expanded-classes',
			open: 'data-mud-craft-core-cookie-open',
			close: 'data-mud-craft-core-cookie-close',
			save: 'data-mud-craft-core-cookie-save',
			type: 'data-mud-craft-core-cookie-type',
			acceptAll: 'data-mud-craft-core-cookie-accept-all',
			rejectAll: 'data-mud-craft-core-cookie-reject-all',
			privacyPolicyLink: 'data-mud-craft-core-cookie-privacy-policy-link',
			detailWrapper: 'data-mud-craft-core-cookie-detail-wrapper',
			toggleDetail: 'data-mud-craft-core-cookie-toggle-detail',
			toggleDetailButtonText: 'data-mud-craft-core-cookie-button-text',
		};

		// ui
		this.wrapper = null;
		this.hiddenClasses = 'hidden';
		this.visibleClasses = '';
		this.isExpandedClasses = 'is-expanded';
		this.blockingMode = false;
		
		// events
		this.events = {
			OPENED: 'mudCookiesOpened',
			CLOSED: 'mudCookiesClosed',
			LOADED: 'mudCookiesLoaded',
			SAVED: 'mudCookiesSaved',
			REPORTED: 'mudCookies'
		};

		// google gtm consent

		this.consentChoices = {
			DENIED: 'denied',
			GRANTED: 'granted',
		};

		this.consentActions = {
			DEFAULT: 'default',
			UPDATE: 'update',
		};

		this.consentTypes = [
			'ad_storage', // Enables storage, such as cookies, related to advertising.
			'ad_user_data', // Sets consent for sending user data to Google for online advertising purposes.
			'ad_personalization', // Sets consent for personalized advertising.
			'analytics_storage', // Enables storage, such as cookies, related to analytics (for example, visit duration).
			'functionality_storage', // Enables storage that supports the functionality of the website or app such as language settings.
			'personalization_storage', // Enables storage related to personalization such as video recommendations.
			'security_storage', // Enables storage related to security such as authentication functionality, fraud prevention, and other user protection
		];

		this.defaultPrereferences = {
			ad_storage: 'denied',
			ad_user_data: 'denied',
			ad_personalization: 'denied',
			analytics_storage: 'denied',
			functionality_storage: 'granted',
			personalization_storage: 'denied',
			security_storage: 'denied',
		};

		// set initial consent
		this.reportToGtm(this.consentActions.DEFAULT);

		// bind scope to the handleEscape handler
		this.handleEscape = this.handleEscape.bind(this);

		// initiate when dom ready
		if (document.readyState === 'complete') this.init();
		else window.addEventListener('DOMContentLoaded', () => this.init());
	}

	/**
	 * Initiates the component when the DOM is ready
	 */
	async init() {
		this.wrapper = document.querySelector(`[${this.dataAttributes.wrapper}]`);
		if (! this.wrapper) this.base.debug('MudCookies: wrapper not found.');
		else await this.loadTemplate();
	}

	/**
	 * Fetches the template markup from the server
	 */
	async loadTemplate() {
		
		await this.base.getSession();

		const res = await this.base.doFetch('/mud/cookies/load', {
			headers: {
				Accept: 'application/json',
				'X-CSRF-Token': this.base.session.csrfTokenValue,
			},
		});
		
		this.blockingMode = res.blockingMode;
		this.handleTemplateLoad(res.markup);
		
	}

	/**
	 * Saves a log of the user preferences to the server
	 *
	 * @param preferences
	 */
	async saveLog(preferences) {

		await this.base.getSession();

		await this.base.doFetch('/mud/cookies/save', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				'X-CSRF-Token': this.base.session.csrfTokenValue,
			},
			body: JSON.stringify({ preferences }),
		});

		this.fireEvent(this.events.SAVED);
		
	}

	/**
	 * Handles the template markup once it has been fetched from the server
	 *
	 * @param markup
	 */
	handleTemplateLoad(markup) {
		this.wrapper.innerHTML = markup;
		this.validateTemplateMarkup();
		this.setPreferencesFromCookie();
		this.addEventListeners();
		this.setUiOptions();
		this.fireEvent(this.events.LOADED);
	}

	/**
	 * Validates the template markup to ensure it contains the required elements
	 */
	validateTemplateMarkup() {
		
		// ensure that the wrapper contains at a minimum, checkboxes for types `functionality_storage` and `analytics_storage`
		const functionality_storage = this.wrapper.querySelector(
			`[${this.dataAttributes.type}="functionality_storage"]`
		);
		const analytics_storage = this.wrapper.querySelector(
			`[${this.dataAttributes.type}="analytics_storage"]`
		);

		// ensure that all checkboxes have valid type attributes
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.type}]`)
			.forEach(el => {
				if (
					!this.consentTypes.includes(el.getAttribute(this.dataAttributes.type))
				)
					this.base.handleError(
						'MudCookies: wrapper contains a checkbox with an invalid type attribute'
					);
			});

		// ensure that the wrapper contains a link to a privacy policy
		const privacyPolicy = this.wrapper.querySelector(
			`[${this.dataAttributes.privacyPolicyLink}]`
		);

		// ensure that the page contains a link to open the cookie controls
		const openLink = document.querySelector(`[${this.dataAttributes.open}]`);

		if (!functionality_storage || !functionality_storage.checked)
			this.base.handleError('MudCookies: wrapper does not contain a checkbox for type `functionality_storage` that is checked by default');
		if (!analytics_storage)
			this.base.handleError('MudCookies: wrapper does not contain a checkbox for type `analytics_storage`');
		if (!privacyPolicy)
			this.base.handleError('MudCookies: wrapper does not contain a link to a privacy policy');
		if (!openLink)
			this.base.handleError('MudCookies: page does not contain a link to open the cookie controls');
	}

	/**
	 * Overrides the UI options from the template markup
	 */
	setUiOptions() {
		// set ui options from data attributes
		const hiddenClasses = this.wrapper.getAttribute(this.dataAttributes.hiddenClasses);
		const visibleClasses = this.wrapper.getAttribute(this.dataAttributes.visibleClasses);
		const isExpandedClasses = this.wrapper.getAttribute(this.dataAttributes.isExpandedClasses);

		if (hiddenClasses) this.hiddenClasses = hiddenClasses;
		if (visibleClasses) this.visibleClasses = visibleClasses;
		if (isExpandedClasses) this.isExpandedClasses = isExpandedClasses;
	}

	/**
	 * Sets the checkboxes in the component to match the user preferences stored in the cookie
	 */
	setPreferencesFromCookie() {
		const preferences = this.base.getCookieValue(this.cookieName);
		if (preferences !== null) {
			Object.keys(preferences).forEach(key => {
				const el = this.wrapper.querySelector(
					`[${this.dataAttributes.type}="${key}"]`
				);
				if (el) el.checked = preferences[key] === this.consentChoices.GRANTED;
			});
		} else {
			this.showComponent();
		}
	}

	/**
	 * Adds event listeners to the page and the component
	 */
	addEventListeners() {
		// open controls in document
		document.querySelectorAll(`[${this.dataAttributes.open}]`).forEach(el => {
			el.addEventListener('click', e => {
				e.preventDefault();
				this.showComponent();
			});
		});

		// checkbox controls in the component
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.type}]`)
			.forEach(el => {
				el.addEventListener('change', () => {
					if (
						el.getAttribute(this.dataAttributes.type) ===
						'functionality_storage'
					)
						el.checked = true;
				});
			});

		// accept all control in the component
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.acceptAll}]`)
			.forEach(el => {
				el.addEventListener('click', async () => {
					this.wrapper
						.querySelectorAll(`[${this.dataAttributes.type}]`)
						.forEach(el => {
							el.checked = true;
						});
					await this.savePreferences();
				});
			});

		// reject all control in the component
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.rejectAll}]`)
			.forEach(el => {
				el.addEventListener('click', async () => {
					this.wrapper
						.querySelectorAll(`[${this.dataAttributes.type}]`)
						.forEach(el => {
							if (
								el.getAttribute(this.dataAttributes.type) !==
								'functionality_storage'
							)
								el.checked = false;
						});
					await this.savePreferences();
				});
			});

		// toggle detail control in the component
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.toggleDetail}]`)
			.forEach(el => {
				el.addEventListener('click', () => {
					const detailWrapper = this.wrapper.querySelector(
						`[${this.dataAttributes.detailWrapper}]`
					);
					this.toggleVisibility(detailWrapper);
					const textTarget = el.querySelector(`[${this.dataAttributes.toggleDetailButtonText}]`) ?? el;
					textTarget.innerText = detailWrapper.classList.contains(this.hiddenClasses) ? 'Show details' : 'Hide details';
				});
			});

		const closeControls = this.wrapper.querySelectorAll(`[${this.dataAttributes.close}]`);
		
		// close controls in the component
		if ( ! this.blockingMode) {
			closeControls.forEach(el => {
				el.addEventListener('click', () => this.hideComponent());
			});
		} else {
			closeControls.forEach(el => {
				el.remove();
			});
		}

		// save controls in the component
		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.save}]`)
			.forEach(el => {
				el.addEventListener('click', () => this.savePreferences());
			});
	}

	/**
	 * Toggles the visibility of an element
	 *
	 * @param el
	 * @param show
	 */
	toggleVisibility(el, show) {
		if (!el) return;
		if (show === true) {
			if (this.hiddenClasses) el.classList.remove(this.hiddenClasses);
			if (this.visibleClasses) el.classList.add(this.visibleClasses);
		} else if (show === false) {
			if (this.hiddenClasses) el.classList.add(this.hiddenClasses);
			if (this.visibleClasses) el.classList.remove(this.visibleClasses);
		} else {
			if (this.hiddenClasses) el.classList.toggle(this.hiddenClasses);
			if (this.visibleClasses) el.classList.toggle(this.visibleClasses);
		}
		// set wrapper expanded class when toggling visibility of detail wrapper
		if (this.isExpandedClasses && typeof el.attributes[this.dataAttributes.detailWrapper] !== 'undefined')  this.wrapper.children[0].classList.toggle(this.isExpandedClasses);
	}

	/**
	 * Shows the cookie component
	 */
	showComponent() {
		this.toggleVisibility(this.wrapper, true);
		this.trapFocus();

		// add escape key listener
		document.addEventListener('keydown', this.handleEscape);
		
		this.fireEvent(this.events.OPENED);
	}

	/**
	 * Calles the hideComponent method if the key was Escape
	 * @param e
	 */
	handleEscape(e) {
		if (e.key === 'Escape') this.hideComponent();
	}

	/**
	 * Hides the cookie component
	 */
	hideComponent() {
		this.base.debug('hideComponent');
		this.toggleVisibility(this.wrapper, false);
		
		// remove escape key listener
		document.removeEventListener('keydown', this.handleEscape);
		
		this.restoreFocus();
		this.fireEvent(this.events.CLOSED);
	}

	/**
	 * Gets all visible focusable elements within the given root
	 */
	getFocusableElements(root) {
		const elements = root.querySelectorAll('a, button, input, textarea, select, [tabindex]:not([tabindex="-1"])');
		return Array.from(elements).filter(el => {
			const style = window.getComputedStyle(el);
			return !!(style.display !== 'none'
				&& style.visibility !== 'hidden'
				&& style.opacity !== '0'
				&& el.offsetWidth > 0
				&& el.offsetHeight > 0);
		});
	}

	/**
	 * Traps the focus within the component
	 */
	trapFocus() {
		// Define the focusable elements selector once
		const focusableSelector = 'button:not([disabled]), [href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), [tabindex]:not([tabindex="-1"])';

		// Remove any existing event listener to prevent duplicates
		if (this.handleFocusTrap) {
			this.wrapper.removeEventListener('keydown', this.handleFocusTrap);
		}

		this.wrapper.addEventListener('keydown', this.handleFocusTrap = (e) => {
			if (e.key === 'Tab') {
				// Get all focusable elements that are visible
				const focusableElements = this.wrapper.querySelectorAll(focusableSelector);

				const visibleFocusableElements = Array.from(focusableElements).filter(el => {
					// Check if element is visible
					const style = window.getComputedStyle(el);
					return style.display !== 'none' && style.visibility !== 'hidden';
				});

				const first = visibleFocusableElements[0];
				const last = visibleFocusableElements[visibleFocusableElements.length - 1];

				// Check if current focus is within modal
				const isWithinModal = Array.from(visibleFocusableElements).includes(document.activeElement);

				if (!isWithinModal) {
					e.preventDefault();
					first.focus();
					this.base.debug('MudCookies: focus not within modal. Forced to first focusable element');
					return;
				}

				if (e.shiftKey) { // shift + tab
					if (document.activeElement === first) {
						e.preventDefault();
						last.focus();
						this.base.debug('MudCookies: focus tabbed out of modal. Forced to last focusable element');
					}
				} else { // tab
					if (document.activeElement === last) {
						e.preventDefault();
						first.focus();
						this.base.debug('MudCookies: focus tabbed out of modal. Forced to first focusable element');
					}
				}
			}
		});

		// Focus the first focusable element when the modal opens
		const firstFocusable = this.wrapper.querySelector(focusableSelector);
		if (firstFocusable) {
			firstFocusable.focus();
			this.base.debug('MudCookies: focus forced to first focusable element');
		}
	}

	/**
	 * Restores the focus to the first focusable element and removes tab trapping
	 */
	restoreFocus() {
		this.wrapper.removeEventListener('keydown', this.handleFocusTrap);
		this.getFocusableElements(document)[0].focus();
		this.base.debug('MudCookies: focus restored');
	}

	/**
	 * Fires a custom event with cookie details and any extra data
	 * 
	 * @param name
	 * @param extra
	 */
	fireEvent(name, extra) {
		const event = new CustomEvent(name, { 
			detail: {
				cookie: this.base.getCookieValue(this.cookieName),
				extra
			}
		});
		
		document.dispatchEvent(event);
	}

	/**
	 * Handles the saving of the user preferences
	 */
	async savePreferences() {
		const preferences = {};

		this.wrapper
			.querySelectorAll(`[${this.dataAttributes.type}]`)
			.forEach(el => {
				const type = el.getAttribute(this.dataAttributes.type);
				preferences[type] = el.checked
					? this.consentChoices.GRANTED
					: this.consentChoices.DENIED;
			});
		
		this.base.setCookie(this.cookieName, preferences, this.cookieExpiry);
		this.fireEvent(this.events.SAVED);
		this.hideComponent();
		this.reportToGtm(this.consentActions.UPDATE);
		await this.saveLog(preferences);
	}

	/**
	 * Reports the user preferences to Google Tag Manager
	 *
	 * @param action
	 */
	reportToGtm(action) {
		const preferences = this.base.getCookieValue(this.cookieName) || {};

		// loop through defaultPreferences and add any missing types
		Object.keys(this.defaultPrereferences).forEach(key => {
			if (!preferences.hasOwnProperty(key))
				preferences[key] = this.defaultPrereferences[key];
		});

		gtag('consent', action, preferences);
		this.fireEvent(this.events.REPORTED, { action });
	}
}

new MudCookies();
