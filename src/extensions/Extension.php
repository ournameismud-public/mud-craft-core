<?php

namespace mud\core\extensions;

use craft\helpers\Template;
use craft\web\twig\TemplateLoaderException;
use mud\core\helpers\Seo;
use mud\core\helpers\Template as Helper;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\Markup;
use Twig\TwigFunction;
use yii\base\Exception;

class Extension extends AbstractExtension
{
    /**
     * @inheritdoc
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('serverInclude', [$this, 'serverInclude']),
            new TwigFunction('dynamicInclude', [$this, 'dynamicInclude']),
            new TwigFunction('cpEditInclude', [$this, 'cpEditInclude']),
            new TwigFunction('enforceCanonicalUrl', [$this, 'enforceCanonicalUrl'])
        ];
    }

    /**
     * Returns markup for a template cached by SSI or ESI
     * Acts as a standard twig include if SSI/ESI is not available
     *
     * @param string $template
     * @param array $params
     * @return Markup
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function serverInclude(string $template, array $params = []): Markup
    {
        return Helper::serverInclude($template, $params);
    }

    /**
     * Returns markup for a dynamic template using Sprig
     *
     * @param string $template
     * @param array $params
     * @param array $config
     * @param string|null $directoryOverride
     * @return Markup
     * @throws LoaderError
     * @throws SyntaxError
     * @throws TemplateLoaderException
     * @throws Exception
     */
    public function dynamicInclude(string $template, array $params = [], array $config = [], string $directoryOverride = null): Markup
    {
        return Helper::dynamicInclude($template, $params, $config, $directoryOverride);
    }

    /**
     * Helper function to include the standard CP edit template for an entry
     *
     * @param int|null $entryId
     * @return Markup
     * @throws Exception
     * @throws LoaderError
     * @throws SyntaxError
     * @throws TemplateLoaderException
     */
    public function cpEditInclude(int $entryId = null): Markup
    {
        return $entryId ? $this->dynamicInclude('cpEdit', ['entryId' => $entryId], ['skeleton' => true], 'mud/includes/') : Template::raw('');
    }
    
    /**
     * Exposes the Seo::enforceCanonicalUrl helper method
     * 
     * @param string $url
     * @return void
     */
    public function enforceCanonicalUrl(string $url): void
    {
        Seo::enforceCanonicalUrl($url);
    }
}