<?php

namespace mud\core\utilities;

use Craft;
use craft\base\Utility;
use mud\core\helpers\ShortUrl as Helper;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Exception;

class ShortUrl extends Utility
{
    /**
     * @inerhitdoc
     */
    public static function displayName(): string
    {
        return Craft::t('mud-craft-core', 'Short Urls');
    }

    /**
     * @inerhitdoc
     */
    public static function id(): string
    {
        return 'short-url';
    }

    /**
     * @inerhitdoc 
     */
    public static function iconPath(): ?string
    {
        return Craft::getAlias('@mud-core/icon.svg');
    }

    /**
     * Loads Short URLs to display in the utility's content HTML
     * 
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public static function contentHtml(): string
    {
        $params = [
            'entriesWithoutShortUrls' => Helper::entriesWithoutShortUrlsQuery()->count()
        ];
        
        return Craft::$app->getView()->renderTemplate('mud-craft-core/short-urls/utility.twig', $params);
    }
}