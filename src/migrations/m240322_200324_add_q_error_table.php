<?php

namespace mud\core\migrations;

use craft\db\Migration;

/**
 * m240322_200324_add_q_error_table migration.
 */
class m240322_200324_add_q_error_table extends Migration
{
    protected Install $installer;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->installer = new Install();
    }

    /**
     * @inheritdoc
     */
    public function safeUp(): bool
    {
        return $this->installer->safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown(): bool
    {
        return $this->installer->safeDown();
    }
}
