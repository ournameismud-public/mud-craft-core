<?php

namespace mud\core\migrations;

use craft\db\Migration;
use craft\db\Table;
use craft\records\Asset;
use craft\records\Element;
use craft\records\Entry;
use craft\records\Site;
use craft\records\User;
use mud\core\records\Blueprint;
use mud\core\records\CookiePreferenceLog;
use mud\core\records\QueueFailureNotification;
use mud\core\records\Redirect;
use mud\core\records\ShortUrl;
use mud\core\records\ShortUrlImpression;
use mud\core\records\VimeoFiles;

class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp(): bool
    {
        if ( ! $this->db->tableExists(CookiePreferenceLog::tableName())) 
        {
            $this->createTable(CookiePreferenceLog::tableName(), [
                'id' => $this->primaryKey(),
                'siteId' => $this->integer()->notNull(),
                'ip' => $this->string()->null(),
                'preferences' => $this->string()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);
        }

        if ( ! $this->db->tableExists(ShortUrl::tableName())) 
        {
            $this->createTable(ShortUrl::tableName(), [
                'id' => $this->primaryKey(),
                'entryId' => $this->integer()->notNull(),
                'code' => $this->string(5)->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, ShortUrl::tableName(), ['entryId'], Element::tableName(), ['id'], 'CASCADE');
            $this->createIndex(null, ShortUrl::tableName(), ['entryId'], true);
            $this->createIndex(null, ShortUrl::tableName(), ['code'], true);
        }

        if ( ! $this->db->tableExists(ShortUrlImpression::tableName()))
        {
            $this->createTable(ShortUrlImpression::tableName(), [
                'id' => $this->primaryKey(),
                'urlId' => $this->integer()->notNull(),
                'siteId' => $this->integer()->notNull(),
                'useragent' => $this->string()->notNull(),
                'userId' => $this->integer()->null(),
                'status' => $this->integer()->notNull(),
                'reason' => $this->string()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, ShortUrlImpression::tableName(), ['urlId'], ShortUrl::tableName(), ['id'], 'CASCADE');
            $this->addForeignKey(null, ShortUrlImpression::tableName(), ['siteId'], Site::tableName(), ['id'], 'CASCADE');
            $this->addForeignKey(null, ShortUrlImpression::tableName(), ['userId'], User::tableName(), ['id'], 'SET NULL');
        }

        if ( ! $this->db->tableExists(QueueFailureNotification::tableName()))
        {
            $this->createTable(QueueFailureNotification::tableName(), [
                'id' => $this->primaryKey(),
                'jobId' => $this->integer()->notNull(),
                'occurrences' => $this->integer()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, QueueFailureNotification::tableName(), ['jobId'], Table::QUEUE, ['id'], 'CASCADE');   
        }

        if ( ! $this->db->tableExists(VimeoFiles::tableName()))
        {
            $this->createTable(VimeoFiles::tableName(), [
                'id' => $this->primaryKey(),
                'assetId' => $this->integer()->notNull(),
                'vimeoId' => $this->integer()->notNull(),
                'data' => $this->text()->notNull(),
                'url' => $this->string()->notNull(),
                'imageUrl' => $this->string(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, VimeoFiles::tableName(), ['assetId'], Asset::tableName(), ['id'], 'CASCADE');
            $this->createIndex(null, VimeoFiles::tableName(), ['assetId'], true);
        }

        if ( ! $this->db->tableExists(Blueprint::tableName()))
        {
            $this->createTable(Blueprint::tableName(), [
                'id' => $this->primaryKey(),
                'sourceId' => $this->integer()->null(),
                'entryId' => $this->integer()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, Blueprint::tableName(), ['sourceId'], Entry::tableName(), ['id'], 'SET NULL');
            $this->addForeignKey(null, Blueprint::tableName(), ['entryId'], Entry::tableName(), ['id'], 'CASCADE');
            $this->createIndex(null, Blueprint::tableName(), ['entryId'], true);
        }

        if ( ! $this->db->tableExists(Redirect::tableName()))
        {
            $this->createTable(Redirect::tableName(), [
                'id' => $this->primaryKey(),
                'siteId' => $this->integer()->notNull(),
                'sourcePath' => $this->string()->notNull(),
                'matchType' => $this->string()->notNull(),
                'destinationPath' => $this->string()->notNull(),
                'httpStatus' => $this->integer()->notNull(),
                'type' => $this->string()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'deleteAt' => $this->dateTime()->null(),
                'uid' => $this->uid()
            ]);

            $this->addForeignKey(null, Redirect::tableName(), ['siteId'], Site::tableName(), ['id'], 'CASCADE');
            $this->createIndex(null, Redirect::tableName(), ['sourcePath'], true);
        }
        
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown(): bool
    {
        if ($this->db->tableExists(QueueFailureNotification::tableName()))
        {
            $this->dropTable(QueueFailureNotification::tableName());
        }
        
        if ($this->db->tableExists(ShortUrlImpression::tableName()))
        {
            $this->dropTable(ShortUrlImpression::tableName());
        }

        if ($this->db->tableExists(ShortUrl::tableName()))
        {
            $this->dropTable(ShortUrl::tableName());
        }
        
        if ($this->db->tableExists(CookiePreferenceLog::tableName())) 
        {
            $this->dropTable(CookiePreferenceLog::tableName());
        }

        if ($this->db->tableExists(VimeoFiles::tableName()))
        {
            $this->dropTable(VimeoFiles::tableName());
        }

        if ($this->db->tableExists(Blueprint::tableName()))
        {
            $this->dropTable(Blueprint::tableName());
        }

        if ($this->db->tableExists(Redirect::tableName()))
        {
            $this->dropTable(Redirect::tableName());
        }
        
        return true;
    }
}
