<?php

namespace mud\core\behaviors;

use Craft;
use craft\elements\Entry;
use craft\fields\Entries;
use craft\helpers\Template;
use Illuminate\Support\Collection;
use mud\core\helpers\Settings;
use mud\core\helpers\Template as Helper;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Markup;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\UnknownPropertyException;

/**
 * @property Entry $owner
 */
class TemplateLibrary extends Behavior
{
    /**
     * Renders the template for each block in a field
     * Matrix field elements will be rendered as a standard include
     * Entries field elements will be rendered as an isolated cached include if enabled
     *
     * @param string $fieldName
     * @param array $extra
     * @param Collection|null $entries
     * @return Markup
     * @throws UnknownPropertyException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function displayBlock(string $fieldName, array $extra = [], Collection $entries = null): Markup
    {
        if ( ! $this->owner->$fieldName || !$this->owner->hasProperty($fieldName)) 
        {
            throw new UnknownPropertyException("Field '$fieldName' not found on Element.");
        }

        $output = '';
        $directory = Settings::config('templatesBlockPath') . '/';
        
        // use a server include if possible where the field is an entries field
        if (Settings::canUseServerIncludes() && Craft::$app->fields->getFieldByHandle($fieldName) instanceof Entries) 
        {
            $variables = [
                'entryId' => $this->owner->id,
                'field' => $fieldName,
                'directory' => $directory,
                'entryIds' => $entries?->pluck('id')->toArray()
            ];

            $output .= Helper::includeTemplate('mud/includes/contentBlockServer', $variables, true);
            
            return Template::raw($output);
        }
        
        $entries = $entries ?: $this->owner->$fieldName->eagerly()->collect();
        $loopIndex = 1;
        
        /** @var Entry $entry */
        foreach ($entries as $entry) 
        {
            // if entry type name ends with Dynamic, load the dynamic include
            $isDynamic = str_ends_with($entry->type->handle, 'Dynamic');
            // if entry type name ends with Server, load the server include
            $isServer = str_ends_with($entry->type->handle, 'Server');
            
            if ($isDynamic || $isServer)
            {
                $variables = array_merge([
                    'parentId' => $this->owner->id,
                    'entryId' => $entry->id,
                    'field' => $fieldName,
                    'blockIndex' => $loopIndex
                ], $extra);

                $output .= $isServer ? 
                    Helper::serverInclude($entry->type->handle, $variables, $directory) : 
                    Helper::dynamicInclude($entry->type->handle, $variables, [ 'skeleton' => true, ], $directory);
            }
            else
            {
                $variables = array_merge([
                    'parent' => $this->owner,
                    'entry' => $entry,
                    'field' => $fieldName,
                    'blockIndex' => $loopIndex
                ], $extra);

                $output .= Helper::includeTemplate($directory . $entry->type->handle, $variables);
            }

            $loopIndex++;
        }
        
        return Template::raw($output);
    }
    
}