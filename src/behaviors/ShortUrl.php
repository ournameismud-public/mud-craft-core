<?php

namespace mud\core\behaviors;

use craft\elements\Entry;
use mud\core\helpers\Settings as Helper;
use mud\core\models\ShortUrl as Model;
use mud\core\Plugin;
use yii\base\Behavior;
use yii\base\Exception;

/**
 * @property Entry $owner
 */
class ShortUrl extends Behavior
{

    /**
     * Determines if the entry has short URL enabled
     * 
     * @return bool
     */
    public function hasShortUrlEnabled(): bool
    {
        foreach (Helper::config('shortUrlSectionAndEntryTypes') as $config)
        {
            $ids = explode(':', $config);
            if (
                $this->owner->siteId === (int) $ids[0] &&
                $this->owner->sectionId === (int) $ids[1] &&
                $this->owner->typeId === (int) $ids[2]
            )
            {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Retrieves the short URL for the entry
     *
     * @return Model|null
     * @throws Exception
     */
    public function shortUrl(): ?Model
    {
        if ( ! $this->hasShortUrlEnabled())
        {
            return null;
        }
        
        return Plugin::getInstance()->shortUrls->findModel([
            'entryId' => $this->owner->id
        ]);
    }
}