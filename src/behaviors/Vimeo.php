<?php

namespace mud\core\behaviors;

use craft\elements\Asset;
use craft\errors\AssetException;
use Exception;
use JsonException;
use mud\core\helpers\Vimeo as Helper;
use mud\core\models\VimeoFile;
use mud\core\models\VimeoFiles;
use mud\core\Plugin;
use yii\base\Behavior;
use yii\base\InvalidConfigException;

/**
 * @property Asset $owner
 */
class Vimeo extends Behavior
{
    /**
     * Retrieves embed data from the asset file content
     * 
     * @return array|null
     * @throws JsonException
     * @throws AssetException
     * @throws InvalidConfigException
     */
    public function embedData(): ?array
    {
        $fileContent = $this->owner->getContents();
        $data = json_decode($fileContent, true, 512, JSON_THROW_ON_ERROR);

        if (! array_key_exists('providerName', $data) || ! array_key_exists('url', $data) || $data['providerName'] !== 'Vimeo')
        {
            return null;
        }
        
        return $data;
    }

    /**
     * Retrieves a VimeoFiles model for the asset
     * 
     * @param VimeoFiles|null $model
     * @return VimeoFiles|null
     * @throws Exception
     */
    public function filesData(VimeoFiles $model = null): ?VimeoFiles
    {
        if ( ! Helper::isEnabled())
        {
            return null;
        }
        
        return $model ?? Plugin::getInstance()->vimeo->findFilesModel([
            'assetId' => $this->owner->id
        ]);
    }
    
    /**
     * Retrieves a single VimeoFile model for the asset, loading the VimeoFiles model if not provided
     * 
     * @param array $conditions
     * @param VimeoFiles|null $model
     * @return VimeoFile|null
     * @throws Exception
     */
    public function fileData(array $conditions, VimeoFiles $model = null): ?VimeoFile
    {
        $model = $this->filesData($model);
        $files = Helper::populateFileModels($model);
        
        return $files->filter(function (VimeoFile $file) use ($conditions) {
            return $file->getAttributes(array_keys($conditions)) === $conditions;
        })->first();
    }
}