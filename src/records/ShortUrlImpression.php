<?php

namespace mud\core\records;

use craft\db\ActiveQuery;
use craft\db\ActiveRecord;
use craft\records\Site;
use craft\records\User;

/**
 * @property int $urlId
 * @property int $siteId
 * @property string $useragent
 * @property int $userId
 * @property int $status
 * @property string $reason
 */
class ShortUrlImpression extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_shorturlimpressions}}';
    }

    /**
     * Defines relationship to ShortUrl
     * 
     * @return ActiveQuery
     */
    public function getShortUrl(): ActiveQuery
    {
        return self::hasOne(ShortUrl::class, ['id' => 'urlId']);
    }

    /**
     * Defines relationship to User
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return self::hasOne(User::class, ['id' => 'userId']);
    }

    /**
     * Defines relationship to Site
     *
     * @return ActiveQuery
     */
    public function getSite(): ActiveQuery
    {
        return self::hasOne(Site::class, ['id' => 'siteId']);
    }
}