<?php

namespace mud\core\records;

use craft\db\ActiveQuery;
use craft\db\ActiveRecord;
use craft\elements\Entry;

/**
 * @property int $entryId
 * @property string $code
 */
class ShortUrl extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_shorturls}}';
    }

    /**
     * Defines relationship to `Entry`
     * 
     * @return ActiveQuery
     */
    public function getEntry(): ActiveQuery
    {
        return self::hasOne(Entry::class, ['id' => 'entryId']);
    }

    /**
     * Defines relationship to `ShortUrlImpression`
     * 
     * @return ActiveQuery
     */
    public function getImpressions(): ActiveQuery
    {
        return self::hasMany(ShortUrlImpression::class, ['urlId' => 'id']);
    }
}
