<?php

namespace mud\core\records;

use craft\db\ActiveRecord;

/**
 * Queue Failure Notification record
 * 
 * @property int $id
 * @property int $jobId
 * @property int $occurrences
 */
class QueueFailureNotification extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_queuefailurenotifications}}';
    }
}
