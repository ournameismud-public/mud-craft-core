<?php

namespace mud\core\records;

use craft\db\ActiveRecord;

/**
 * Cookie Preference Log record
 *
 * @property int $siteId
 * @property string $ip
 * @property string $preferences
 */
class CookiePreferenceLog extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_cookiepreferencelog}}';
    }
}