<?php

namespace mud\core\records;

use craft\db\ActiveRecord;
use DateTime;

/**
 * @property int $siteId
 * @property string $sourcePath
 * @property string $matchType
 * @property string $destinationPath
 * @property int $httpStatus
 * @property string $type
 * @property DateTime $deleteAt
 */
class Redirect extends ActiveRecord
{
    public const MATCH_TYPE_EXACT = 'exact';
    public const MATCH_TYPE_REGEX = 'regex';
    public const TYPE_USER = 'user';
    public const TYPE_SYSTEM = 'system';
    
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_redirects}}';
    }
}