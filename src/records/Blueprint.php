<?php

namespace mud\core\records;

use craft\db\ActiveQuery;
use craft\db\ActiveRecord;
use craft\records\Entry;

/**
 * @property int $sourceId
 * @property int $entryId
 */
class Blueprint extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_blueprints}}';
    }

    /**
     * Defines relationship to source `Entry`
     *
     * @return ActiveQuery
     */
    public function getSource(): ActiveQuery
    {
        return self::hasOne(Entry::class, ['id' => 'sourceId']);
    }

    /**
     * Defines relationship to created `Entry` record
     *
     * @return ActiveQuery
     */
    public function getEntry(): ActiveQuery
    {
        return self::hasOne(Entry::class, ['id' => 'entryId']);
    }
}