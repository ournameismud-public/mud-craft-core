<?php

namespace mud\core\records;

use craft\db\ActiveQuery;
use craft\db\ActiveRecord;
use craft\elements\Asset;

/**
 * @property int $assetId
 * @property int $vimeoId
 * @property string $data
 * @property string $url
 * @property string $imageUrl
 */
class VimeoFiles extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%mud_vimeofiles}}';
    }

    /**
     * Defines relationship to `Asset`
     * 
     * @return ActiveQuery
     */
    public function getEntry(): ActiveQuery
    {
        return self::hasOne(Asset::class, ['id' => 'assetId']);
    }
}
