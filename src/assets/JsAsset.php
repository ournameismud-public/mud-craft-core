<?php

namespace mud\core\assets;

use Craft;
use craft\web\AssetBundle;
use Exception;
use RuntimeException;

abstract class JsAsset extends AssetBundle
{
    /**
     * @var string|null The name of the JS file to include in the asset bundle.
     */
    protected string|null $fileName = null;

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function init(): void
    {
        if ( ! $this->fileName)
        {
            throw new RuntimeException('You must set the $fileName property in your asset class.');
        }
        
        $this->sourcePath = '@mud-core/resources';

        // https://www.jetbrains.com/help/phpstorm/minifying-javascript.html#ws_js_minify_example_uglifyJS
        $this->js = [
            Craft::$app->config->general->devMode ? "js/$this->fileName.js" : "js/$this->fileName.min.js",
        ];

        parent::init();
    }
}