<?php

namespace mud\core\assets;

use Craft;
use craft\web\AssetBundle;

class ControlPanel extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init(): void
    {
        $templatesPath = Craft::getAlias('@templates');
        $assetPath = '/_mud-core/css/control-panel.css';
        
        if ( ! Craft::$app->request->isCpRequest || ! file_exists($templatesPath . $assetPath))
        {
            return;
        }
        
        $this->sourcePath = '@templates' . $assetPath;
        
        $this->css = [
            'css/control-panel.css',
        ];
        
        parent::init();
    }
}