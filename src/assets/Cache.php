<?php

namespace mud\core\assets;

use Craft;
use craft\web\AssetBundle;

class Cache extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init(): void
    {
        $this->sourcePath = '@mud-core/resources';
        
        // https://www.jetbrains.com/help/phpstorm/minifying-javascript.html#ws_js_minify_example_uglifyJS
        $this->js = [
            Craft::$app->config->general->devMode ? 'js/cache.js' : 'js/cache.min.js',
        ];
        
        parent::init();
    }
}