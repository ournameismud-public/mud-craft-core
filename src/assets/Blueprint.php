<?php

namespace mud\core\assets;

use craft\web\AssetBundle;

class Blueprint extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init(): void
    {
        $this->sourcePath = '@mud-core/resources';
        
        $this->css = [
            'css/blueprint.css'
        ];
        
        parent::init();
    }
}