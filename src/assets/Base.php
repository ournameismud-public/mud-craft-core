<?php

namespace mud\core\assets;

class Base extends JsAsset
{
    protected string|null $fileName = 'base';
}