<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;
use mud\core\helpers\Settings;
use mud\core\helpers\ShortUrl as Helper;
use yii\base\Exception;

class ShortUrl extends Model
{
    public ?int $id = null;
    
    /**
     * @var int The entry ID the code is associated with
     */
    public int $entryId;
    
    /**
     * @var string The shortcode in the URL of the cade
     */
    public string $code;

    /**
     * @var string|null The site URL of the code 
     */
    public ?string $url = null;

    /**
     * @var string|null The data URI of the QR code
     */
    public ?string $dataUri = null;

    /**
     * @var int|null The size of the QR code
     */
    public ?int $size = null;
    
    /**
     * @var DateTime|null The date the code was created
     */
    public ?DateTime $dateCreated = null;
    
    /**
     * @var DateTime|null The date the code was updated
     */
    public ?DateTime $dateUpdated = null;
    
    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['entryId'], 'number'];
        $rules[] = [['entryId', 'code'], 'required'];

        return $rules;
    }

    /**
     * Adds derived properties to the model
     *
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $config = [])
    {
        if ($config['code'])
        {
            $config['url'] = Helper::url($config['code']);
            $config['dataUri'] = Helper::generateQrCode($config['url']);
            $config['size'] = Settings::config('shortUrlQrCodeImageSize');
        }
        
        parent::__construct($config);
    }
}
