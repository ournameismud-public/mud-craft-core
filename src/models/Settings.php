<?php

namespace mud\core\models;

use craft\base\Model;

class Settings extends Model
{
    /**
     * ============== Cookie settings ============== 
     */

    /**
     * @var bool Whether to enable cookies functionality
     */
    public bool $cookiesEnabled = true;
    
    /**
     * @var bool Whether to log the IP address of the user when they save their cookie preferences
     */
    public bool $logIpAddressForCookies = true;

    /**
     * @var string The path to the cookies wrapper template
     */
    public string $cookiesWrapperTemplatePath = 'mud-craft-core/cookies/_wrapper';

    /**
     * @var string The path to the cookies modal template
     */
    public string $cookiesModalTemplatePath = 'mud-craft-core/cookies/_consent';

    /**
     * @var bool Whether to display the cookies modal in blocking mode
     */
    public bool $cookiesDisplayBlockingMode = false;

    /**
     * ============== Short URL settings ==============
     */

    /**
     * The combined section and entry type IDs enabled for short URL generation
     * Format = [ siteId:sectionId:entryTypeId ]
     * 
     * @var array 
     * @see ShortUrl::hasShortUrlEnabled()
     */
    public array $shortUrlSectionAndEntryTypes = [];
    
    /**
     * @var string The path to create the QR code short URL with
     */
    public string $shortUrlPath = 's';

    /**
     * @var int The size of the QR code image
     */
    public int $shortUrlQrCodeImageSize = 250;

    /**
     * @var int The margin around the QR code image
     */
    public int $shortUrlQrCodeImageMargin = 10;
    
    /**
     * @var string The foreground colour of the QR code image
     */
    public string $shortUrlQrCodeForegroundColour = '#000000';
    
    /**
     * @var string The background colour of the QR code image
     */
    public string $shortUrlQrCodeBackgroundColour = '#ffffff';
    
    /**
     * @var string The path to the logo image
     * TODO: validate this is a valid image?
     */
    public string $shortUrlQrCodeLogoPath = '';
    
    /**
     * @var int The size of the logo image
     */
    public int $shortUrlQrCodeLogoSize = 20;

    /**
     * @var bool Whether impressions should be logged
     */
    public bool $logShortUrlImpressions = true;

    /**
     * ============== Template settings ==============
     */

    /**
     * @var string Path to templates loaded by displayBlock() behavior
     */
    public string $templatesBlockPath = '_entryTypes/blocks';

    /**
     * @var string Path to templates loaded by serverInclude extension method
     */
    public string $templatesServerIncludePath = '_includes';

    /**
     * @var string Path to sprig templates loaded by dynamicInclude() extension method
     */
    public string $templatesDynamicIncludePath = '_sprig';

    /**
     * @var bool Whether to enable cookies functionality
     */
    public bool $cacheNoticeEnabled = true;

    /**
     * @var string Path to template loaded for the cache notice
     */
    public string $cacheNoticeTemplatePath = 'mud-craft-core/cache/_notice';

    /**
     * ============== Vimeo settings ==============
     */

    /**
     * @var bool Enable the Vimeo API integration
     */
    public bool $enableVimeoApi = false;

    /**
     * ============== Blueprint settings ==============
     */
    
    /**
     * @var array The blueprint section IDs
     */
    public array $blueprintSectionIds = [];
    
    /**
     * @var array The blueprint entry type IDs
     */
    public array $blueprintEntryTypeIds = [];
    
    /**
     * @var array The blueprint field IDs
     */
    public array $blueprintThumbnailFieldIds = [];
    
    /**
     * @inheritDoc
     */
    public function defineRules(): array
    {
        return [
            [
                [
                    'cookiesWrapperTemplatePath', 
                    'cookiesModalTemplatePath', 
                    'shortUrlPath', 
                    'shortUrlQrCodeImageSize',
                    'shortUrlQrCodeImageMargin',
                    'shortUrlQrCodeForegroundColour',
                    'shortUrlQrCodeBackgroundColour',
                    'templatesBlockPath',
                    'templatesServerIncludePath',
                    'templatesDynamicIncludePath',
                    'cacheNoticeTemplatePath'
                ], 
                'required'
            ],
            [
                [
                    'shortUrlQrCodeImageSize',
                    'shortUrlQrCodeImageMargin',
                    'shortUrlQrCodeLogoSize',
                ], 
                'integer'
            ],
            [
                [
                    'shortUrlQrCodeImageSize',
                ],
                'integer',
                'min' => 100
            ]
        ];
    }
}