<?php

namespace mud\core\models;

use craft\base\Model;

class CookiePreferenceLog extends Model
{
    /**
     * @var int Site ID user accessed
     */
    public int $siteId;
    
    /**
     * @var string IP address of user
     */
    public string $ip;
    
    /**
     * @var string Cookie choices made by user
     */
    public string $preferences;

    /**
     * @inheritdoc
     */
    public function defineRules(): array
    {
        return [
            [['siteId', 'preferences'], 'required'],
            [['siteId'], 'number'],
        ];
    }
}