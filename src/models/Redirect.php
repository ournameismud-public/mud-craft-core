<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;
use mud\core\records\Redirect as Record;

class Redirect extends Model
{
    public int $id = 0;

    /**
     * @var int The site ID
     */
    public int $siteId = 0;

    /**
     * @var string The source path
     */
    public string $sourcePath = '';
    
    /**
     * @var string The type of match (exact or regex)
     */
    public string $matchType = Record::MATCH_TYPE_EXACT;
    
    /**
     * @var string The destination path
     */
    public string $destinationPath = '';
    
    /**
     * @var int The HTTP status code
     */
    public int $httpStatus = 301;
    
    /**
     * @var string The type of redirect (user or system)
     */
    public string $type = Record::TYPE_USER;
    
    /**
     * @var DateTime|null The date the record was created
     */
    public ?DateTime $dateCreated = null;

    /**
     * @var DateTime|null The date the record was updated
     */
    public ?DateTime $dateUpdated = null;

    /**
     * @var DateTime|null The date the record should be deleted
     */
    public ?DateTime $deleteAt = null;

    public ?string $uid = null;

    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['sourcePath', 'matchType', 'destinationPath', 'type'], 'string'];
        $rules[] = [['siteId', 'httpStatus'], 'integer'];
        $rules[] = [['siteId', 'sourcePath', 'destinationPath'], 'required'];
        $rules[] = ['matchType', 'in', 'range' => [Record::MATCH_TYPE_EXACT, Record::MATCH_TYPE_REGEX]];
        $rules[] = ['type', 'default', 'value' => 'exact'];
        $rules[] = ['type', 'in', 'range' => [Record::TYPE_USER, Record::TYPE_SYSTEM]];
        $rules[] = ['type', 'default', 'value' => 'user'];
        $rules[] = ['httpStatus', 'in', 'range' => [301, 302, 307, 308, 410]];
        $rules[] = ['httpStatus', 'default', 'value' => 301];

        return $rules;
    }
}