<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;

class Blueprint extends Model
{
    public ?int $id = null;

    /**
     * @var int The source entry ID (i.e. the blueprint entry)
     */
    public int $sourceId;
    
    /**
     * @var int The entry ID the entry created from the source entry
     */
    public int $entryId;
    
    /**
     * @var DateTime|null The date the record was created
     */
    public ?DateTime $dateCreated = null;

    /**
     * @var DateTime|null The date the record was updated
     */
    public ?DateTime $dateUpdated = null;

    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['sourceId', 'entryId'], 'number'];
        $rules[] = [['sourceId', 'entryId'], 'required'];

        return $rules;
    }
}