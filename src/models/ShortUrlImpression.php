<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;

class ShortUrlImpression extends Model
{
    /**
     * @var int|null The ID of the short URL
     */
    public ?int $urlId = null;

    /**
     * @var int|null Site ID
     */
    public ?int $siteId = null;

    /**
     * @var int|null Logged in user ID
     */
    public ?int $userId = null;

    /**
     * @var int|null Request status code
     */
    public ?int $status = null;

    /**
     * @var string|null Request status text
     */
    public ?string $reason = null;

    /**
     * @var string|null Request user agent
     */
    public ?string $useragent = null;

    /**
     * @var DateTime|null The date the impression was created
     */
    public ?DateTime $dateCreated = null;

    /**
     * @var DateTime|null The date the impression was updated
     */
    public ?DateTime $dateUpdated = null;

    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['urlId', 'siteId', 'userId'], 'number'];
        $rules[] = [['urlId', 'siteId', 'useragent'], 'required'];

        return $rules;
    }
}