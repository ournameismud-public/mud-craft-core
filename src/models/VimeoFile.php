<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;

class VimeoFile extends Model
{
    /**
     * @var int|null The record ID.
     */
    public ?int $id = null;
    
    /**
     * @var int|null The asset ID.
     */
    public ?int $assetId;

    /**
     * @var int|null The Vimeo ID.
     */
    public ?int $vimeoId;

    /**
     * @var string|null The quality (i.e. hls).
     */
    public ?string $quality = null;
    
    /**
     * @var string|null The rendition (i.e. 1080p).
     */
    public ?string $rendition = null;
    
    /**
     * @var string|null The type (i.e. video/mp4).
     */
    public ?string $type = null;
    
    /**
     * @var int|null The width in pixels.
     */
    public ?int $width = null;
    
    /**
     * @var int|null The height in pixels.
     */
    public ?int $height = null;
    
    /**
     * @var string|null The link to the file.
     */
    public ?string $link = null;
    
    /**
     * @var int|null The bitrate.
     */
    public ?int $fps = null;
    
    /**
     * @var int|null The filesize.
     */
    public ?int $size = null;
    
    /**
     * @var string|null MD5 string.
     */
    public ?string $md5 = null;
    
    /**
     * @var string|null The public name.
     */
    public ?string $public_name = null;
    
    /**
     * @var string|null The size summary.
     */
    public ?string $size_short = null;
    
    /**
     * @var DateTime|null When the video was created in Vimeo.
     */
    public ?DateTime $created_time = null;

    /**
     * @var DateTime|null The date the record was created
     */
    public ?DateTime $dateCreated = null;

    /**
     * @var DateTime|null The date the record was updated
     */
    public ?DateTime $dateUpdated = null;

    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['assetId', 'vimeoId'], 'number'];
        $rules[] = [['assetId', 'vimeoId', 'link', 'type', 'rendition'], 'required'];

        return $rules;
    }
}
