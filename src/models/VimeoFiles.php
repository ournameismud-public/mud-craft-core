<?php

namespace mud\core\models;

use craft\base\Model;
use DateTime;

class VimeoFiles extends Model
{
    /**
     * @var int|null The record ID
     */
    public ?int $id = null;
    
    /**
     * @var int|null The asset ID the data is associated with
     */
    public ?int $assetId;
    
    /**
     * @var int|null The vimeo ID the data is associated with 
     */
    public ?int $vimeoId;

    /**
     * @var string|null The json data 
     */
    public ?string $data;

    /**
     * @var string|null The URL to the video on Vimeo
     */
    public ?string $url;

    /**
     * @var string|null The video image URL
     */
    public ?string $imageUrl;
    
    /**
     * @var DateTime|null The date the record was created
     */
    public ?DateTime $dateCreated = null;
    
    /**
     * @var DateTime|null The date the record was updated
     */
    public ?DateTime $dateUpdated = null;
    
    /**
     * @inheritDoc
     */
    protected function defineRules(): array
    {
        $rules = parent::defineRules();

        $rules[] = [['assetId', 'vimeoId'], 'number'];
        $rules[] = [['assetId', 'vimeoId', 'data', 'url'], 'required'];

        return $rules;
    }
}
