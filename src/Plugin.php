<?php

namespace mud\core;

use Craft;
use craft\base\Model;
use craft\base\Plugin as BasePlugin;
use mud\core\extensions\Extension;
use mud\core\helpers\Assets;
use mud\core\helpers\Events;
use mud\core\helpers\Hooks;
use mud\core\helpers\Log;
use mud\core\models\Settings;
use mud\core\services\Blueprint;
use mud\core\services\Cookies;
use mud\core\services\Queue;
use mud\core\services\Redirect;
use mud\core\services\Reporting;
use mud\core\services\ShortUrlImpressions;
use mud\core\services\ShortUrls;
use mud\core\services\Slack;
use mud\core\services\Vimeo;
use putyourlightson\sprig\Sprig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Exception;
use yii\base\InvalidConfigException;

/**
 * @property-read Blueprint $blueprint
 * @property-read Cookies $cookies
 * @property-read Queue $queue
 * @property-read Redirect $redirect
 * @property-read Reporting $reporting
 * @property-read ShortUrls $shortUrls
 * @property-read ShortUrlImpressions $shortUrlImpressions
 * @property-read Slack $slack
 * @property-read Vimeo $vimeo
 */
class Plugin extends BasePlugin
{
    /**
     * @inheritdoc
     */
    public string $schemaVersion = '1.1.0';
    
    /**
     * @inheritdoc
     */
    public static function config(): array
    {
        return [
            'components' => [
                'blueprint' => Blueprint::class,
                'cookies' => Cookies::class,
                'queue' => Queue::class,
                'redirect' => Redirect::class,
                'reporting' => Reporting::class,
                'shortUrls' => ShortUrls::class,
                'shortUrlImpressions' => ShortUrlImpressions::class,
                'slack' => Slack::class,
                'vimeo' => Vimeo::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function createSettingsModel(): ?Model
    {
        return new Settings();
    }

    /**
     * @inerhitdoc 
     */
    public bool $hasCpSettings = true;

    /**
     * Loads plugin settings template
     * 
     * @return string|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    protected function settingsHtml(): ?string
    {
        return Craft::$app->getView()->renderTemplate('mud-craft-core/settings', [ 
            'settings' => $this->getSettings() 
        ]);
    }

    /**
     * Initializes the plugin
     * 
     * @return void
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        Craft::setAlias('@mud-core', __DIR__);

        $this->controllerNamespace = Craft::$app->request->isConsoleRequest ? 'mud\\core\\console\\controllers' : 'mud\\core\\controllers';

        Log::create('mud-core', 'mud\core\*');
        Assets::registerAssetBundles();
        Events::listen();
        Hooks::intercept();
        Sprig::bootstrap();

        Craft::$app->getView()->registerTwigExtension(new Extension());
    }
}