<?php

namespace mud\core\helpers;

use Craft;
use craft\web\View;
use mud\core\assets\Base;
use mud\core\assets\Blueprint;
use mud\core\assets\Cache;
use mud\core\assets\ControlPanel;
use mud\core\assets\Cookies;
use yii\base\InvalidConfigException;

class Assets
{
    /**
     * Registers asset bundles
     * 
     * @return void
     * @throws InvalidConfigException
     */
    public static function registerAssetBundles(): void
    {
        if (Craft::$app->getRequest()->isConsoleRequest)
        {
            return;
        }

        if (Craft::$app->getRequest()->isCpRequest)
        {
            Craft::$app->view->registerAssetBundle(ControlPanel::class);
            Craft::$app->view->registerAssetBundle(Blueprint::class);
            return;
        }

        $cacheNoticeEnabled = Settings::config('cacheNoticeEnabled');
        $cookiesEnabled = Settings::config('cookiesEnabled');
        
        if ( ! $cacheNoticeEnabled && ! $cookiesEnabled)
        {
            return;
        }

        Craft::$app->view->registerAssetBundle(Base::class, View::POS_HEAD);

        if ($cacheNoticeEnabled)
        {
            Craft::$app->view->registerAssetBundle(Cache::class);
        }
        
        if ($cookiesEnabled)
        {
            Craft::$app->view->registerAssetBundle(Cookies::class, View::POS_HEAD);   
        }
    }
}