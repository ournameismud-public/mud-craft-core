<?php

namespace mud\core\helpers;

use mud\core\models\Redirect as Model;
use mud\core\records\Redirect as Record;
use RuntimeException;
use yii\db\Exception;

class Redirect
{
    /**
     * Finds a redirect model by its source path
     * 
     * @param int $siteId
     * @param string $sourcePath
     * @param string|null $matchType
     * @return Model|null
     */
    public static function findExactMatch(int $siteId, string $sourcePath, string $matchType = null):? Model
    {
        $model = new Model([
            'siteId' => $siteId,
            'sourcePath' => $sourcePath
        ]);
        
        $record = Record::findOne($model->getAttributes(['siteId', 'sourcePath']));
        
        return $record ? new Model($record->getAttributes()) : null;
    }

    /**
     * Creates a redirect
     * 
     * @param Model $model
     * @return Model
     * @throws Exception
     */
    public static function create(Model $model): Model
    {
        if ($model->id)
        {
            throw new RuntimeException('Cannot create a redirect model that already has an ID.');
        }
        
        if ( ! $model->validate())
        {
            throw new RuntimeException('Failed to validate redirect model.');
        }
        
        $record = new Record($model->getAttributes());
        
        if ( ! $record->save())
        {
            throw new RuntimeException('Failed to save redirect record.');
        }
        
        $model->setAttributes($record->getAttributes(), false);
        
        return $model;
    }

    /**
     * Creates a redirect for a deleted blueprint placeholder entry
     * 
     * @param int $siteId
     * @param string $url
     * @return void
     * @throws Exception
     */
    public static function createBlueprintPlaceholderRedirect(int $siteId, string $url): void
    {
        $path = preg_replace('/^https?:\/\/[^\/]+/', '', $url);
        $destination = preg_replace('/\/\d+.*$/', '', $path);
        
        if ( ! self::findExactMatch($siteId, $path))
        {
            $model = new Model([
                'siteId' => $siteId,
                'sourcePath' => $path,
                'destinationPath' => $destination,
                'type' => Record::TYPE_SYSTEM,
                'deleteAt' => date('Y-m-d H:i:s', strtotime('+1 day'))
            ]);
            
            self::create($model);
        }
    }

    /**
     * Deletes old redirect records
     * 
     * @return void
     */
    public static function deleteOldRecords(): void
    {
        Record::deleteAll(['<', 'deleteAt', date('Y-m-d H:i:s')]);
    }
}