<?php

namespace mud\core\helpers;

use Craft;
use craft\elements\Entry;
use craft\errors\InvalidElementException;
use craft\errors\UnsupportedSiteException;
use Illuminate\Support\Collection;
use mud\core\models\Blueprint as Model;
use mud\core\records\Blueprint as Record;
use RuntimeException;
use Throwable;
use yii\db\Exception;

class Blueprint
{
    /**
     * Duplicates an entry as a draft into a given section and structure location
     * 
     * @param Entry $source
     * @param int $authorId
     * @param int $sectionId
     * @param int|null $structureId
     * @param int|null $parentId
     * @return Entry
     * @throws InvalidElementException
     * @throws Throwable
     * @throws UnsupportedSiteException
     */
    public static function duplicateEntry(Entry $source, int $authorId, int $sectionId, int $structureId = null, int $parentId = null): Entry
    {
        return Craft::$app->elements->duplicateElement($source, [
            'dateCreated' => new \DateTime(),
            'authorId' => $authorId,
            'sectionId' => $sectionId,
            'structureId' => $structureId,
            'parentId' => $parentId,
            'isProvisionalDraft' => false,
            'draftId' => null,
            'slug' => null
        ], false, true);
    }
    
    /**
     * Duplicates a source entry into a target entry's section and removes the blueprint thumbnail
     * 
     * @param Entry $source
     * @param Entry $target
     * @param int $authorId
     * @return Entry
     * @throws Throwable
     * @throws InvalidElementException
     * @throws UnsupportedSiteException
     */
    public static function createEntryFromBlueprint(Entry $source, Entry $target, int $authorId): Entry
    {
        $entry = self::duplicateEntry($source, $authorId, $target->sectionId, $target->structureId, $target->parentId);
        
        if ( ! $thumbnailFieldIds = Settings::config('blueprintThumbnailFieldIds'))
        {
            return $entry;
        }
        
        if (($thumbnailFieldId = $thumbnailFieldIds[$target->siteId]) && ($thumbnailFieldHandle = Craft::$app->fields->getFieldById($thumbnailFieldId)?->handle) && $entry->$thumbnailFieldHandle) 
        {
            $entry->$thumbnailFieldHandle = null;
            Craft::$app->elements->saveElement($entry, false, false, false);
        }
        
        return $entry;
    }
 
    /**
     * Creates a blueprint record
     * 
     * @param int $sourceId
     * @param int $entryId
     * @return Record
     * @throws Exception
     */
    public static function createRecord(int $sourceId, int $entryId): Record
    {
        $model = new Model([
            'sourceId' => $sourceId,
            'entryId' => $entryId
        ]);
        
        if ( ! $model->validate())
        {
            throw new RuntimeException('Failed to validate blueprint model.');
        }
        
        $record = new Record($model->getAttributes());
        
        if ( ! $record->save())
        {
            throw new RuntimeException('Failed to save blueprint record.');
        }
        
        return $record;
    }

    /**
     * Retrieves a blueprint entry from which an entry was created
     * 
     * @param int|null $entryId
     * @return Entry|null
     */
    public static function entryBlueprint(int $entryId = null): ?Entry
    {
        $record = Record::findOne([
            'entryId' => $entryId
        ]);
        
        return $record && $record->sourceId ? Entry::findOne($record->sourceId) : null;
    }

    /**
     * Retrieves IDs of all entries created from a blueprint
     * 
     * @param Entry $blueprint
     * @return Collection
     */
    public static function blueprintEntries(Entry $blueprint): Collection
    {
        return collect(Record::findAll(['sourceId' => $blueprint->id]))->pluck('entryId');
    }

    /**
     * Retrieves the blueprint section ID configured for the current site
     * 
     * @return int|null
     */
    public static function getSectionId(): ?int
    {
        $config = Settings::config('blueprintSectionIds');
        return $config && $config[Craft::$app->sites->currentSite->id] ? (int) $config[Craft::$app->sites->currentSite->id] : null;
    }

    /**
     * Retrieves the blueprint entry type ID configured for the current site
     * 
     * @return int|null
     */
    public static function getEntryTypeId(): ?int
    {
        $config = Settings::config('blueprintEntryTypeIds');
        return $config && $config[Craft::$app->sites->currentSite->id] ? (int) $config[Craft::$app->sites->currentSite->id] : null;
    }

    /**
     * Returns whether the current user has permission to manage blueprints
     * 
     * @return bool
     */
    public static function userHasPermission(): bool
    {
        return Craft::$app->user->checkPermission('manageBlueprints');
    }
}