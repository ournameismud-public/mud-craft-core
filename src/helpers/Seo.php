<?php

namespace mud\core\helpers;

use Craft;
use nystudio107\seomatic\models\Settings as SeomaticSettings;
use nystudio107\seomatic\Seomatic;
use RuntimeException;

class Seo
{
    /**
     * Enforces SEO-optimised canonical and robots configuration for URLs with query params
     *
     * Sets canonical to only include a pagination (`page`) param if it's not the default (1)
     * and removes canonical tag and header if there are other query params
     *
     * Sets `robots` tag and header to `noindex, follow` if the canonical is removed
     *
     * See the following for more information:
     * https://developers.google.com/search/docs/crawling-indexing/consolidate-duplicate-urls
     * https://www.searchenginejournal.com/google-dont-mix-noindex-relcanonical/262607/
     * https://github.com/nystudio107/craft-seomatic/blob/develop-v5/src/config.php#L131
     * https://github.com/nystudio107/craft-seomatic/blob/develop-v5/src/config.php#L173
     *
     * @param string $url
     * @return void
     */
    public static function enforceCanonicalUrl(string $url): void
    {
        if ( ! Settings::pluginIsInstalled('seomatic'))
        {
            throw new RuntimeException('This method relies on SEOmatic being installed and enabled.');
        }

        /** @var Seomatic $seomatic */
        $seomatic = Craft::$app->plugins->getPlugin('seomatic');
        
        /** @var SeomaticSettings $settings */
        $settings = $seomatic->getSettings();

        if ( ! in_array('page', $settings->allowedUrlParams, true))
        {
            throw new RuntimeException('The `page` URL parameter must be added to the `allowedUrlParams` array in the `./config/seomatic.php` config file.');
        }

        if ($settings->alwaysIncludeCanonicalUrls)
        {
            throw new RuntimeException('The `alwaysIncludeCanonicalUrls` option in the `./config/seomatic.php` config file must be set to `false`.');
        }

        Seomatic::$seomaticVariable->tag->container()->clearCache = true;

        $queryParams = Craft::$app->request->getQueryParams();

        if (array_diff_key($queryParams, ['page' => '']))
        {
            Seomatic::$seomaticVariable->meta->canonicalUrl = '';
            Seomatic::$seomaticVariable->meta->robots = 'noindex, follow';
            return;
        }

        $page = $queryParams['page'] ?? 1;
        
        Seomatic::$seomaticVariable->meta->canonicalUrl = (int) $page > 1 ? "$url?page=$page" : $url;
    }
}