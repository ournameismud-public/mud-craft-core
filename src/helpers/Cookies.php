<?php

namespace mud\core\helpers;

use Craft;
use craft\errors\SiteNotFoundException;
use JsonException;
use mud\core\models\CookiePreferenceLog;

class Cookies
{
    /**
     * Populates a new cookie preference log model
     * 
     * @param array $preferences
     * @return CookiePreferenceLog
     * @throws SiteNotFoundException
     * @throws JsonException
     */
    public static function populateCookiePreferenceLogModel (array $preferences): CookiePreferenceLog
    {
        $ip = Settings::config('logIpAddressForCookies');
        
        return new CookiePreferenceLog([
            'siteId' => Craft::$app->getSites()->getCurrentSite()->id,
            'ip' => $ip,
            'preferences' => json_encode($preferences, JSON_THROW_ON_ERROR)
        ]);
    }
}