<?php

namespace mud\core\helpers;

use Craft;
use craft\base\Model;
use mud\core\Plugin;
use RuntimeException;

class Settings
{
    /**
     * Retrieves a setting from the plugin config
     * 
     * @param string|null $key
     * @return Model|mixed|null
     */
    public static function config(string $key = null): mixed
    {
        if ( ! $plugin = Plugin::getInstance())
        {
            throw new RuntimeException('Unable to retrieve plugin instance');
            
        }
        
        if (! $config = $plugin->getSettings())
        {
            throw new RuntimeException('Unable to retrieve plugin settings');
        }
        
        return $key ? $config[$key] : $config;
    }

    /**
     * Determines if a plugin is installed and enabled
     * 
     * @param string $pluginHandle
     * @return bool
     */
    public static function pluginIsInstalled(string $pluginHandle): bool
    {
        return Craft::$app->plugins->isPluginInstalled($pluginHandle) && Craft::$app->plugins->isPluginEnabled($pluginHandle);
    }

    /**
     * Determines if Blitz static includes can be used
     * by evaluating presence of the plugin and its settings
     *
     * @return bool
     */
    public static function canUseServerIncludes(): bool
    {
        if ( ! self::pluginIsInstalled('blitz'))
        {
            return false;
        }

        $blitz = Craft::$app->plugins->getPlugin('blitz');

        if ( ! $blitz || (float) $blitz->version < 5.4 || ! $settings = $blitz->getSettings())
        {
            return false;
        }
        
        $attributes = $settings->getAttributes();

        return ! ( ! $attributes['cachingEnabled'] || ! ($attributes['ssiEnabled'] || $attributes['esiEnabled']));
    }
}