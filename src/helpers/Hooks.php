<?php

namespace mud\core\helpers;

use Craft;
use mud\core\helpers\Template as Helper;

class Hooks
{
    /**
     * Intercepts defined template hooks
     * 
     * @return void
     */
    public static function intercept(): void
    {
        Craft::$app->getView()->hook('mud-cookie-consent', static function()
        {
            return Settings::config('cookiesEnabled') ? Helper::loadTemplateFromConfig('cookiesWrapperTemplatePath') : '';
        });

        Craft::$app->getView()->hook('mud-cache-notice', static function()
        {
            return Settings::config('cacheNoticeEnabled') ? Helper::loadTemplateFromConfig('cacheNoticeTemplatePath') : '';
        });
    }
}