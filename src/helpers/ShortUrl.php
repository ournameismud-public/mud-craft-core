<?php

namespace mud\core\helpers;

use Craft;
use craft\elements\db\EntryQuery;
use craft\elements\Entry;
use craft\helpers\UrlHelper;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode;
use Endroid\QrCode\Writer\PngWriter;
use mud\core\models\ShortUrl as Model;
use mud\core\Plugin;
use mud\core\records\ShortUrl as Record;
use RuntimeException;
use yii\base\Exception;

class ShortUrl
{
    /**
     * Creates a unique 5 letter string
     * 
     * @return string
     */
    public static function code(): string
    {
        $code = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, 5);
        return Record::find()->where([ 'code' => $code ])->exists() ? self::code() : $code;
    }

    /**
     * Creates a unique URL for a code
     *
     * @param string $code
     * @return string
     * @throws Exception
     */
    public static function url(string $code): string
    {
        $path = Settings::config('shortUrlPath');
        return UrlHelper::siteUrl("$path/$code");
    }

    /**
     * Builds a Short URL model
     *
     * @param Entry $entry
     * @return Model
     * @throws Exception
     */
    public static function createModelFromEntry(Entry $entry): Model
    {
        $model = new Model([
            'entryId' => $entry->canonicalId,
            'code' => self::code()
        ]);
        
        if ( ! $model->validate())
        {
            throw new RuntimeException('Short URL model is not valid.');
        }
        
        return $model;
    }

    /**
     * Creates a Short URL model from a record
     *
     * @param Record $record
     * @return Model
     * @throws Exception
     */
    public static function createModelFromRecord(Record $record): Model
    {
        return new Model($record->getAttributes(null, ['uid']));
    }

    /**
     * Converts a hex color to an RGB color
     * 
     * @param $hexColor
     * @return Color
     */
    protected static function hexToColor($hexColor): Color 
    {
        // Remove '#' if it was added
        $hexColor = ltrim($hexColor, '#');

        // If the color code is shorthand (e.g., #FFF), expand it to the full format (e.g., #FFFFFF)
        if (strlen($hexColor) === 3) 
        {
            $hexColor = $hexColor[0] . $hexColor[0] . $hexColor[1] . $hexColor[1] . $hexColor[2] . $hexColor[2];
        }

        [ $red, $green, $blue ] = sscanf($hexColor, "%02x%02x%02x");

        return new Color($red, $green, $blue);
    }

    /**
     * Generates a QR Code
     * 
     * @param string $data
     * @return string
     * @throws Exception
     */
    public static function generateQrCode(string $data): string
    {
        $writer = new PngWriter();
        $logo = null;
        $foreground = self::hexToColor(Settings::config('shortUrlQrCodeForegroundColour'));
        $background = self::hexToColor(Settings::config('shortUrlQrCodeBackgroundColour'));
        
        $qrCode = QrCode::create('Data')
            ->setEncoding(new Encoding('UTF-8'))
			->setErrorCorrectionLevel(ErrorCorrectionLevel::Low)
            ->setSize(Settings::config('shortUrlQrCodeImageSize'))
            ->setMargin(Settings::config('shortUrlQrCodeImageMargin'))
			->setRoundBlockSizeMode(RoundBlockSizeMode::Margin)
            ->setForegroundColor($foreground)
            ->setBackgroundColor($background)
            ->setData($data);
        
        if (Settings::config('shortUrlQrCodeLogoPath'))
        {
            $logoPath = Craft::$app->path->getConfigPath() . Settings::config('shortUrlQrCodeImageSize');
            $logoSize = Settings::config('shortUrlQrCodeLogoSize');
            
            if ( ! is_file($logoPath) || ! getimagesize($logoPath))
            {
                throw new RuntimeException('QR code logo path is not a valid image file.');
            }
            
            $logo = Logo::create($logoPath)->setResizeToWidth($logoSize);
        }

        return $logo ? $writer->write($qrCode, $logo)->getDataUri() : $writer->write($qrCode)->getDataUri();
    }

    /**
     * Builds a query for any entries without a Short URL
     *
     * @return EntryQuery
     * @throws Exception
     */
    public static function entriesWithoutShortUrlsQuery(): EntryQuery
    {
        $ids = array_merge(['not'], Plugin::getInstance()->shortUrls->getAll()->pluck('entryId')->toArray());
        
        $query = Entry::find()
            ->status(null)
            ->limit(null)
            ->site('*')
            ->id($ids);

        $clauses = [];
        
        foreach (Settings::config('shortUrlSectionAndEntryTypes') as $config)
        {
            if ( ! $config)
            {
                continue;
            }
            
            $ids = explode(':', $config);
            $clauses[] = ['elements_sites.siteId' => $ids[0], 'sectionId' => $ids[1], 'typeId' => $ids[2]];
        }
        
        if ($clauses)
        {
            array_unshift($clauses, 'or');
            return $query->andWhere($clauses);
        }
        
        // no sections are enabled, so we want an empty result set
        return $query->andWhere(['typeId' => -1]);
    }
}