<?php

namespace mud\core\helpers;

use craft\base\Element;
use craft\base\Model;
use craft\base\Plugin as BasePlugin;
use craft\elements\Address;
use craft\elements\Asset;
use craft\elements\Category;
use craft\elements\Entry;
use craft\elements\GlobalSet;
use craft\elements\Tag;
use craft\elements\User;
use craft\events\AuthorizationCheckEvent;
use craft\events\DefineBehaviorsEvent;
use craft\events\DefineHtmlEvent;
use craft\events\IndexKeywordsEvent;
use craft\events\ModelEvent;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterElementSourcesEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\events\RegisterUserPermissionsEvent;
use craft\queue\Queue;
use craft\services\Elements;
use craft\services\Entries;
use craft\services\Gc;
use craft\services\Search;
use craft\services\Sites;
use craft\services\UserPermissions;
use craft\services\Utilities;
use craft\web\UrlManager;
use craft\web\View;
use mud\core\behaviors\ShortUrl as ShortUrlBehavior;
use mud\core\behaviors\TemplateLibrary as TemplateLibraryBehavior;
use mud\core\behaviors\Vimeo as VimeoBehavior;
use mud\core\Plugin;
use mud\core\utilities\ShortUrl as ShortUrlUtility;
use yii\base\Application;
use yii\base\Event;
use yii\base\Model as YiiModel;
use yii\queue\ExecEvent;
use yii\queue\Queue as YiiQueue;

class Events
{
    /**
     * List of core Elements that have behaviors
     * Category, GlobalSet and Tag added for completeness but generally
     * no longer used in Craft 5 builds
     * 
     * @var array
     */
    protected static array $behaviorElements = [
        Address::class,
        Asset::class,
        Category::class,
        Entry::class,
        GlobalSet::class,
        Tag::class,
        User::class
    ];
    
    /**
     * Registers event listeners
     * 
     * @return void
     */
    public static function listen(): void
    {
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            static function (RegisterUrlRulesEvent $event) 
            {
                $event->rules['mud/cookies/load'] = 'mud-craft-core/cookies/load-template';
                $event->rules['mud/cookies/save'] = 'mud-craft-core/cookies/save-preferences-log';
                $shortUrlPath = Settings::config('shortUrlPath');
                $event->rules[$shortUrlPath . '/<code:\w+>'] = 'mud-craft-core/short-url';
                $event->rules['mud/cache/session'] = 'mud-craft-core/cache/get-user-permissions';
            }
        );
        
        Event::on(
            View::class,
            View::EVENT_REGISTER_SITE_TEMPLATE_ROOTS,
            static function(RegisterTemplateRootsEvent $event) 
            {
                $event->roots['mud'] = __DIR__ . '/../templates';
            }
        );

        Event::on(
            View::class,
            View::EVENT_REGISTER_CP_TEMPLATE_ROOTS,
            static function(RegisterTemplateRootsEvent $event)
            {
                $event->roots['mud'] = __DIR__ . '/../templates';
            }
        );

        Event::on(
            Queue::class,
            YiiQueue::EVENT_AFTER_ERROR,
            static function (ExecEvent $event)
            {
                Plugin::getInstance()->queue->handleError($event);
            }
        );

        Event::on(
            Entry::class,
            Element::EVENT_AFTER_SAVE,
            static function(Event $event)
            {
                /** @var Entry $entry */
                $entry = $event->sender;
                Plugin::getInstance()->shortUrls->generateEntryUrl($entry);
            }
        );

        Event::on(
            Asset::class,
            Element::EVENT_AFTER_SAVE,
            static function (ModelEvent $event)
            {
                /** @var Asset $asset */
                $asset = $event->sender;
                Plugin::getInstance()->vimeo->handleAssetSave($asset);
            }
        );
        
        Event::on(
            Entry::class,
            Element::EVENT_DEFINE_SIDEBAR_HTML,
            static function(DefineHtmlEvent $event)
            {
                Plugin::getInstance()->shortUrls->appendEntrySidebar($event);
                Plugin::getInstance()->blueprint->appendEntrySidebar($event);
            }
        );

        Event::on(
            Asset::class,
            Element::EVENT_DEFINE_SIDEBAR_HTML,
            static function (DefineHtmlEvent $event)
            {
                Plugin::getInstance()->vimeo->appendAssetSidebar($event);
            }
        );

        Event::on(
            Entry::class,
            Element::EVENT_DEFINE_ADDITIONAL_BUTTONS,
            static function (DefineHtmlEvent $event) 
            {
                Plugin::getInstance()->blueprint->appendEntryButtons($event);
            }
        );

        Event::on(
            Plugin::class,
            BasePlugin::EVENT_AFTER_SAVE_SETTINGS,
            static function () 
            {
                Plugin::getInstance()->shortUrls->generateAllEntryUrls();
            }
        );
        
        Event::on(
            Sites::class,
            Sites::EVENT_AFTER_SAVE_SITE,
            static function () 
            {
                Plugin::getInstance()->shortUrls->generateAllEntryUrls();
            }
        );
        
        Event::on(
            Entries::class,
            Entries::EVENT_AFTER_SAVE_SECTION,
            static function () {
                Plugin::getInstance()->shortUrls->generateAllEntryUrls();
            }
        );

        Event::on(
            Utilities::class,
            Utilities::EVENT_REGISTER_UTILITIES,
            static function (RegisterComponentTypesEvent $event) 
            {
                $event->types[] = ShortUrlUtility::class;
            }
        );

        /*
         * Registers behaviors for all core Elements
         * To extend this to custom Elements or plugin Elements that an individual site  
         * uses, register an equivalent event listener in the site's mud module
         */
        foreach (self::$behaviorElements as $element)
        {
            Event::on(
                $element,
                Model::EVENT_DEFINE_BEHAVIORS,
                static function(DefineBehaviorsEvent $event) use ($element)
                {
                    if ($element === Entry::class)
                    {
                        $event->behaviors['mud-short-url'] = ShortUrlBehavior::class;
                    }

                    if ($element === Asset::class)
                    {
                        $event->behaviors['mud-vimeo'] = VimeoBehavior::class;
                    }

                    $event->behaviors['mud-templates'] = TemplateLibraryBehavior::class;
                }
            );
        }

        Event::on(
            Entry::class,
            YiiModel::EVENT_AFTER_VALIDATE,
            static function (Event $event) 
            {
                Plugin::getInstance()->blueprint->handleEntryValidation($event);
            }
        );

        Event::on(
            Search::class,
            Search::EVENT_BEFORE_INDEX_KEYWORDS,
            static function (IndexKeywordsEvent $event) 
            {
                Plugin::getInstance()->blueprint->excludeFromSearch($event);
            }
        );

        Event::on(
            Application::class,
            Application::EVENT_BEFORE_REQUEST,
            static function (Event $event) 
            {
                Plugin::getInstance()->redirect->handleRequest($event);
            }
        );
        
        Event::on(
            View::class,
            View::EVENT_BEFORE_RENDER_PAGE_TEMPLATE,
            static function () 
            {
                Plugin::getInstance()->blueprint->checkViewPermissions();
            }
        );

        Event::on(
            Gc::class,
            Gc::EVENT_RUN,
            static function() 
            {
                Plugin::getInstance()->redirect->handleGarbageCollection();
            }
        );

        Event::on(
            UserPermissions::class,
            UserPermissions::EVENT_REGISTER_PERMISSIONS,
            static function(RegisterUserPermissionsEvent $event) 
            {
                Plugin::getInstance()->blueprint->registerPermissions($event);
            }
        );

        Event::on(
            Elements::class,
            Elements::EVENT_AUTHORIZE_VIEW,
            static function (AuthorizationCheckEvent $event) 
            {
                Plugin::getInstance()->blueprint->authoriseView($event);
            }
        );

        Event::on(
            Entry::class,
            Element::EVENT_REGISTER_SOURCES,
            static function (RegisterElementSourcesEvent $event) 
            {
                Plugin::getInstance()->blueprint->sanitiseEntrySources($event);
            }
        );
    }
}