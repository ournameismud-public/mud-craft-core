<?php

namespace mud\core\helpers;

use Craft;
use craft\elements\Asset;
use craft\elements\Entry;
use craft\helpers\App;
use craft\helpers\Template as CraftTemplateHelper;
use craft\helpers\UrlHelper;
use JsonException;
use mud\core\elements\EntryVeneer;
use mud\core\models\VimeoFiles;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Markup;
use yii\base\Exception;

class Template
{
    /**
     * Renders a cached include or a regular include based site settings
     *
     * @param string $template
     * @param array $params
     * @param bool $serverInclude
     * @return string
     * @throws LoaderError
     * @throws SyntaxError
     */
    public static function includeTemplate(string $template, array $params = [], bool $serverInclude = false): string
    {
        $twig = $serverInclude ? "{{ craft.blitz.includeCached(template, params) }}" : "{% include template with params only %}";

        $variables = [
            'params' => $params,
            'template' => $template,
            'sprig' => false
        ];

        Craft::info('Rendering template: ' . $template . '. Using server include: ' . ($serverInclude ? 'yes.' : 'no.'), __METHOD__);

        return Craft::$app->view->renderString($twig, $variables);
    }

    /**
     * Returns markup for a dynamic template using Sprig
     *
     * @param string $template
     * @param array $params
     * @param array $config
     * @param string|null $directoryOverride
     * @return Markup
     * @throws Exception
     * @throws LoaderError
     * @throws SyntaxError
     */
    public static function dynamicInclude(string $template, array $params = [], array $config = [], string $directoryOverride = null): Markup
    {
        $directory = $directoryOverride ?? Settings::config('templatesDynamicIncludePath') . '/';

        // allow override of default config
        $config = array_merge([
            's-trigger' => 'refresh',
            'skeleton' => false,
            'id' => uniqid($template, true),
            'class' => "dynamic-${template}"
        ], $config);

        // but force trigger if skeleton is enabled and environment has not been set to override that
        if ($config['skeleton'] === true)
        {
            $config['s-trigger'] = App::env('FORCE_SKELETON_UI') ? 'refresh' : 'refresh, load';
        }

        $variables = [
            'template' => $directory.$template,
            'params' => $params,
            'config' => $config
        ];

        return CraftTemplateHelper::raw(self::includeTemplate('mud/includes/dynamic', $variables));
    }

    /**
     * Returns markup for a template cached by SSI or ESI
     * Acts as a standard twig include if SSI/ESI is not available
     *
     * @param string $template
     * @param array $params
     * @param string|null $directoryOverride
     * @return Markup
     * @throws LoaderError
     * @throws SyntaxError
     */
    public static function serverInclude(string $template, array $params = [], string $directoryOverride = null): Markup
    {
        $enabled = Settings::canUseServerIncludes();
        $directory = $directoryOverride ?: Settings::config('templatesServerIncludePath') . '/';
        $variables = array_merge(['params' => $params], ['template' => $directory . $template]);

        return CraftTemplateHelper::raw(self::includeTemplate('mud/includes/server', $variables, $enabled));
    }
    
    /**
     * Renders a template using a config value which is either loaded from the plugin's config file,
     * or from an override set in the site's config/mud-craft-core.php file
     *
     * @param string $configKey
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public static function loadTemplateFromConfig(string $configKey): string
    {
        $path = Settings::config($configKey);
        return Craft::$app->view->renderTemplate($path);
    }

    /**
     * Displays a short URL in a CP sidebar block
     *
     * @param Entry $entry
     * @return string
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function entryShortUrlSidebarTemplate(Entry $entry): string
    {
        /* @var EntryVeneer $entry */
        $shortUrl = $entry->shortUrl();  /** @phpstan-ignore-line */
        
        $params = [
            'entry' => $entry,
            'shortUrl' => $shortUrl
        ];

        return Craft::$app->getView()->renderTemplate('mud-craft-core/short-urls/sidebar.twig', $params);
    }

    /**
     * Displays Vimeo files data in a CP sidebar block
     *
     * @param Asset $asset
     * @param int|null $vimeoId
     * @param VimeoFiles|null $model
     * @return string
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws JsonException
     */
    public static function vimeoFilesSidebarTemplate(Asset $asset, int $vimeoId = null, VimeoFiles $model = null): string
    {
        $params = [
            'asset' => $asset,
            'url' => $model?->url,
            'imageUrl' => $model?->imageUrl,
            'vimeoId' => $vimeoId,
            'files' => Vimeo::populateFileModels($model),
            'dateCreated' => $model?->dateCreated
        ];
        
        return Craft::$app->getView()->renderTemplate('mud-craft-core/vimeo/sidebar.twig', $params);
    }

    /**
     * Displays blueprint details in a CP sidebar block
     * 
     * @param Entry $entry
     * @return string
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function blueprintSidebarTemplate(Entry $entry): string
    {
        $params = [
            'blueprint' => $entry,
            'entries' => Blueprint::blueprintEntries($entry)
        ];

        return Craft::$app->getView()->renderTemplate('mud-craft-core/blueprints/sidebar.twig', $params);
    }

    /**
     * Displays a button to view an entry's source blueprint
     * 
     * @param Entry $blueprint
     * @return string
     */
    public static function blueprintViewSourceButtonTemplate(Entry $blueprint): string
    {
        return '<a href="' . $blueprint->cpEditUrl . '" class="blueprint-btn btn">View Blueprint</a>';
    }

    /**
     * Displays a button to create a blueprint from an entry
     * 
     * @param Entry $entry
     * @return string
     */
    public static function blueprintCreateFromEntryButtonTemplate(Entry $entry): string
    {
        return '<a href="' . UrlHelper::actionUrl('mud-craft-core/blueprint/create-blueprint-from-entry', [ 'sourceId' => $entry->id ]) . '" class="blueprint-btn btn">Create Blueprint from Entry</a>';
    }
}