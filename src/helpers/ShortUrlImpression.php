<?php

namespace mud\core\helpers;

use Craft;
use craft\web\Request;
use mud\core\models\ShortUrl as ShortUrlModel;
use mud\core\models\ShortUrlImpression as Model;
use RuntimeException;
use Throwable;

class ShortUrlImpression
{
    /**
     * Creates a new ShortUrlImpression model from page request data
     * 
     * @param ShortUrlModel $shortUrl
     * @param int $siteId
     * @param int $status
     * @param string $reason
     * @param Request $request
     * @return Model
     * @throws Throwable
     */
    public static function createModelFromRequest(ShortUrlModel $shortUrl, int $siteId, int $status, string $reason, Request $request): Model
    {
        $user = Craft::$app->getUser()->getIdentity();
        
        $model = new Model([
            'urlId' => $shortUrl->id,
            'siteId' => $siteId,
            'userId' => $user->id ?? null,
            'status' => $status,
            'reason' => $reason,
            'useragent' => $request->userAgent,
        ]);
        
        if ( ! $model->validate())
        {
            throw new RuntimeException('ShortUrlModelImpression validation failed.');
        }
        
        return $model;
    }
}