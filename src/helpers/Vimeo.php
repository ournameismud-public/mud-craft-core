<?php

namespace mud\core\helpers;

use Craft;
use Illuminate\Support\Collection;
use JsonException;
use mud\core\models\VimeoFile;
use mud\core\models\VimeoFiles;
use mud\core\records\VimeoFiles as Record;
use RuntimeException;

class Vimeo
{
    /**
     * Checks if the Vimeo API is enabled in plugin settings 
     * and that the Embedded Assets plugin is installed and enabled
     * 
     * @return bool
     */
    public static function isEnabled(): bool
    {
        return Settings::config('enableVimeoApi')
            && Craft::$app->plugins->isPluginInstalled('embeddedassets') 
            && Craft::$app->plugins->isPluginEnabled('embeddedassets');
    }
    
    /**
     * Creates a Vimeo Files Data model from a record
     *
     * @param Record $record
     * @return VimeoFiles
     */
    public static function createFilesModelFromRecord(Record $record): VimeoFiles
    {
        return new VimeoFiles($record->getAttributes(null, ['uid']));
    }

    /**
     * Populates a VimeoFiles model
     * 
     * @param int $assetId
     * @param int $vimeoId
     * @param array $embedData
     * @param array $filesData
     * @return VimeoFiles
     * @throws JsonException
     */
    public static function populateFilesModel(int $assetId, int $vimeoId, array $embedData, array $filesData): VimeoFiles
    {
        $model = new VimeoFiles([
            'assetId' => $assetId,
            'vimeoId' => $vimeoId,
            'data' => json_encode($filesData, JSON_THROW_ON_ERROR),
            'url' => $embedData['url'],
            'imageUrl' => $embedData['image']
        ]);

        if (!$model->validate()) 
        {
            throw new RuntimeException('VimeoFiles model is not valid.');
        }

        return $model;
    }

    /**
     * Populates a VimeoFile model
     * 
     * @param array $data
     * @param int $assetId
     * @param int $vimeoId
     * @return VimeoFile
     */
    public static function populateFileModel(array $data, int $assetId, int $vimeoId): VimeoFile
    {
        $data['assetId'] = $assetId;
        $data['vimeoId'] = $vimeoId;
        $model = new VimeoFile($data);

        if (!$model->validate()) 
        {
            throw new RuntimeException('VimeoFile model is not valid.');
        }

        return $model;
    }

    /**
     * Populates a Collection of VimeoFile models
     * 
     * @param VimeoFiles|null $model
     * @return Collection
     * @throws JsonException
     */
    public static function populateFileModels(VimeoFiles $model = null): Collection
    {
        $files = [];
        
        if ($model)
        {
            $data = json_decode($model->data, true, 512, JSON_THROW_ON_ERROR);

            if(array_key_exists('files', $data))
            {
                foreach ($data['files'] as $file)
                {
                    $files[] = self::populateFileModel($file, $model->assetId, $model->vimeoId);
                }
            }
        }
        
        return collect($files);
    }

    /**
     * Gets the Vimeo ID from a Vimeo URL
     *
     * @param string $url
     * @return int|null
     */
    public static function getVimeoIdFromUrl(string $url): ?int
    {
        $pattern = '/vimeo\.com\/(\d+)$/i';

        if (preg_match($pattern, $url, $matches)) {
            return (int) $matches[1];
        }

        return null;
    }
}